Please view this file on the master branch, on stable branches it's out of date.

### 2019-01-14
- New: Users page, blazing fast, with search bar, filters and pagination
- New: Infinite loading in projects & groups page + in all feeds
- New: Modal of people who clapped a post
- Tweak: Improved design for list of users in modals (followers, following, members..)
- Tweak: Now load 12 items at a time (page projects & groups)
- Tweak: Separate objects into tabs, inside user following modal (https://gitlab.com/JOGL/JOGL/issues/247)
- Tweak: Improved profile page header on mobiles
- Fix: Post content inside () being deleted on submit
- Tweak: Sticky nav bar to the top, on user profile page
- Tweak: Show only 6 sdgs by default on an object edition (and can toggle to view more/mess)

### 2019-12-07
- New: Refreshed design for the "need" object
- New: added 12 new default profile images
- New: on all feeds : get only a few elements from the api & add a load more button to view more
- Tweak: change emails "from name" to "JOGL - Just One Giant Lab" (instead of JOGL) + "from email" to "noreply@" (instead of hello@)
- Tweak: changed max size for banners and profile image from 10Mo to 4Mo
- Tweak: allow only png/jpg as banner or profile images
- Tweak: remove unnecessary objects from most api calls (speed improvement)
- Tweak: now order projects/groups/needs in the backend (so no need to filter them on front anymore)
- Tweak: added pagination in consequence (to get 5 most recent projets/groups.. for ex)
- Fix: sort comments by id, so have most recent on top
- Fix: fixed list component (ul/li) not working in quill js editor

### 2019-12-02
- New: Add notification when someone posts on your challenge or program
- New: Added more field on the project object (to guide you detailing it even more if you want)
- Tweak: All quill js links will now open on new window (https://gitlab.com/JOGL/JOGL/issues/158)
- Tweak: on all about pages, detect links outside of <a> tag and linkify them
- Tweak: improve user profile header’s style on really small devices
- Tweak: don't trigger email if you commented on your profile
- Tweak: don't trigger email if you comment on a post in which you are mentioned
- Tweak: now redirect to correct tabs depending on hash of a profile page
- Tweak: in emails, change wording community to group
- Tweak: replace due date by 'urgent' in the need
- Tweak: in the confirmation email, add full confirmation link , if the button doesn’t work
- Tweak: change email content if user mentioned someone in a post or pot’s comment on his profile
- Tweak: don't trigger email if you posted or commented in an object you are admin/owner
- Tweak: change email content when someone you follow post in their own profile (else they are posting in other object you follow)
- Fix: Home feed not working if you followed deleted users/projects..
- Fix: bug when switching to another tab in an object edition page

### 2019-11-21
- New: Added needs in menu and display latest needs on homepage
- New: tabs on mobile homepage, to switch between the "feed" and "what's new" sections
- Tweak: specify on a project header if it's participating to a challenge
- Tweak: open upload to all file types!
- Tweak: Make user bio go full width if user is not connected (on desktop)
- Tweak: add '...' if text is longer than 2 lines (on members cards, and on projects/groups small cards on homepage)
- Tweak: Style improvements for really small devices (header menu + homepage)
- Fix: On the 'someone followed you' mail, redirect the button to the person who followed you, not you.

### 2019-11-19
- New: New notification emails: when someone follows you, when someone post in an object you follow (user, project, challenge..).
- New: homepage (when connected) loads only 10 posts and you can load more with button (improve performance)
- Tweak: Speed improvements through many pages
- Tweak: now specify which user mentioned or posted about you in notification emails.
- Tweak: Design enchancements on needs page
- Tweak: view all user's skills when clicking on the "+.." on the user's profile header
- Tweak: better display documents on feed posts
- Fix: signin modal not closing when clicking the button
- Fix: language switcher not displaying selected language

### 2019-11-18
- New: You can now contact users on the platform! (if they chose to be publicly contacted)
- New: Brand new notification emails design!
- New: New design when listing members on the platform (people page, member modal popups..) + can contact users from modals as well.
- New: added 8 default profile images that are chosen randomly on signup (if you don't upload a profile picture)
- New: Now trigger a "please sign in" modal on any 401 error (except on sign in page)
- New: Add a page with the needs from all the projects (/needs)
- New: Now can edit and add documents to an old post
- New: Now can delete documents on your project/challenge
- New: New people page that shows only 100 users at a time and have a "load more button"
- Tweak: Those pages now load much faster:  projects, challenges, communities and homepage.
- Tweak: Revised wording, subject, and content for all email notifications
- Tweak: Dynamic hash in url that goes to matching tab, and update when you select another tab
- Tweak: Added smooth scrolling when jumping to other tab, sections..
- Tweak: More precise error messages on signin + display button to resend confirmation link if you didn’t validate your account
- Tweak: Improve the modal "Add Project"
- Tweak: Automatically follow a need when creating it
- Fix: On the homepage feed, posts from « needs » object now redirect to the need’s tab section of its project page
- Fix: various fixes on the feed
- Fix: various minor fixes

### 2019-10-18
- New: Add changelog
- New: User can now download their data on the settings page
- New: new design with fewer and sticky tabs for normal and edition pages of the following object: project, user, groups, programs & challenges
- New: Dynamic no result message
- New: Remove followers and members tab (project, groups, users), now accessible by clicking the numbers in header
- Tweak: only display city and country for address
- Tweak: revised settings page design
- Tweak: code optimization
- Tweak: better design for document cards
- Tweak: now anyone can post or comment on a need
- Tweak: hide documents tab is user is not admin and they are no docs
- Tweak: added needs in the project edition page
- Tweak: anyone can comment a need (no need to click I'll help)
- Tweak: Better wording for need object and other places
- Tweak: Display number of posts and comments on the need card
- Tweak: Display confirmation message on member role change

### 2019-10-11 ([MR 167](https://gitlab.com/JOGL/frontend-v0.1/merge_requests/167))
- New: some fields for challenges and program now have french translation
- New: you can now archive or delete a project  (https://gitlab.com/JOGL/JOGL/issues/195))
- New: insert emoji directly within our text editor  (https://gitlab.com/JOGL/JOGL/issues/200)
- Tweak: show need count on projects cards
- Tweak: added a button to show/hide some fields in the edition pages
- Tweak: don’t show draft challenges anywhere, show them only when published  (https://gitlab.com/JOGL/JOGL/issues/226)
- Tweak: code optimisation and cleaning
- Tweak: show more results in search bar and better readability
- Tweak: add gitlab link to contribute to the code
- Tweak: add MIT license to the code  (https://gitlab.com/JOGL/JOGL/issues/241)
- Fix: problem that prevent changing role to owner in french langage
- Fix: pending users not working + unability to join projects (err 403)
- Fix: minor problems


### 2019-09-12 ([MR 156](https://gitlab.com/JOGL/frontend-v0.1/merge_requests/156))
- New: introducing the searchbar on JOGL (searching into skills, names, descriptions... of projects and people) 🔎  (https://gitlab.com/JOGL/JOGL/issues/39)
- New: you can now filter people, projects and groups by alphabetical order, by dates or by popularity
- New: Need object  (https://gitlab.com/JOGL/JOGL/issues/64) / (https://gitlab.com/JOGL/JOGL/issues/65) / (https://gitlab.com/JOGL/JOGL/issues/67)
- Tweak: add short bio to user profile
- Tweak: new header for challenges
- Tweak: participate dropdown on challenge header
- Tweak: better navigation on mobile devices
- Tweak: new design for search bar and mobile dropdown menu
- tweak: add link to create project & link to rules on add project modal
- Fix: some security issues
- Fix: minor problems

### 2019-08-19
- New: Add a chatbot!
- New: ability to drag and drop + copy/paste + resize images in the quill text editor
- Tweak: Add translations + revised the pages: data, terms/conditions, ethic/pledge.
- Tweak: save the page you were before signing in
- Fix security issues
- Fix: minor improvements

### 2019-07-31
- New: add language dropdown on header + help link on user menu
- Tweak: show comments by default and add button to show more than 4
- Tweak: translation ethic pledge page
- Tweak: little design enhancement of profile page
- translate home intro + enhance style for mobile
- Fix: mentions that made the whole text a link

### 2019-07-24
- Tweak: improve design of creation and edition page (responsive)
- Tweak: some pages have now dynamic tab titles
- Tweak: all website now translated
- Tweak: at some places on website, show only few skills and interests, and add a button to view more
