import React, { Component, Fragment } from "react";
import Api from "Api";
import { injectIntl } from "react-intl";
import ProjectList from "Components/Projects/ProjectList";
import ProjectsHeader from "../../Components/Projects/ProjectsHeader";
import Loading from "Components/Tools/Loading";
import Filter from "Components/Tools/Filter";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
import { UserContext } from 'UserProvider';
import { infiniteLoader } from 'Components/Tools/utils/utils.js'
//import ProjectMap from "Components/Projects/ProjectMap";

class ProjectsPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      listProjects: [],
      loading: true,
			loadingBtn: false,
			loadBtnCount: 1,
			hideLoadBtn: false,
    };
  }

  myCallback = dataFromChild => {
    this.setState({ listDataFromChild: dataFromChild }); // send data to filter component (child)
	};
	
	loadProjects(currentPage) {
		var itemsPerQuery = 12 // number of users per query calls (load more btn click)
		if(currentPage > 1) this.setState({loadingBtn: true});
		Api.get(`/api/projects?items=${itemsPerQuery}&page=${currentPage}&order=desc`)
		.then(res => {
			var listProjects = this.state.listProjects;
			var projects = res.data
			projects.map((project) => { // map	
				return listProjects.push(project);
			});
			this.setState({listProjects, loading: false, loadingBtn: false, loadBtnCount: currentPage += 1});
			infiniteLoader() // relaunch function at each call
    })
    .catch(error => {
			// console.log(error);
			this.state.hideLoadBtn = true // hide btn when it's last call
      this.setState({ loading: false, loadingBtn: false });
		});
	}

  componentDidMount(){
		this.setState({loading: true});
		this.loadProjects(this.state.loadBtnCount)
  }

  render() {
		const { listProjects, loading, loadingBtn, hideLoadBtn } = this.state;
		const { intl } = this.props;
		var btnHiddenClass = hideLoadBtn ? "d-none" : ""
		var showFilterClass = !hideLoadBtn ? "d-none" : ""
		let userContext = this.context;

		// console.log(this.state.loading);
		// if(!this.state.loading && !this.state.hideLoadBtn) {
		// 	setTimeout(() => {
		// 		window.onscroll = function() {
		// 			// console.log(this.state.loading);
		// 			infiniteLoader()
		// 		}; // redo function on scroll
		// 	}, 2000);
		// }

    // Dynamic SEO meta tags
    var SEO_title =
      intl.formatMessage({ id: "projects.header.title" }) + " - JOGL";
    var SEO_desc = intl.formatMessage({ id: "projects.header.description" });
    document
      .querySelector('meta[property="og:image"]')
      .setAttribute(
        "content",
        window.location.origin +
          require("assets/img/default/default-project.jpg")
      );
    document
      .querySelector('meta[property="og:title"]')
      .setAttribute("content", SEO_title);
    document
      .querySelector('meta[name="description"]')
      .setAttribute("content", SEO_desc);
    document
      .querySelector('meta[property="og:description"]')
      .setAttribute("content", SEO_desc);

    return (
      <div className="container-fluid">
        <Helmet>
          <title>{SEO_title}</title>
          {/* <meta name="title" content={SEO_title} data-react-helmet="true" />
					<meta name="description" content={SEO_desc} data-react-helmet="true" />
					<meta property="og:title" content={SEO_title} data-react-helmet="true" /> */}
          {/* <meta property="og:image" content={project.banner_url} data-react-helmet="true" /> */}
        </Helmet>
        {/*<ProjectMap />*/}
        <ProjectsHeader />
				{/* start - Filter and create button */}
				<div className="row justify-content-around projectsSearch">
	        <div className="col-6">
	        	<div className={`${showFilterClass}`}>
							<Filter list={listProjects} callback={this.myCallback} />
						</div>
	        </div>
	        <div className="col-6 text-right">
						{userContext.connected &&
	            <Link to="/project/create">
	              <button className="btn btn-sm btn-primary btn-action">
									{intl.formatMessage({ id: "projects.searchBar.btnAddProject" })}
	              </button>
	            </Link>
	          }
	        </div>
	      </div>
				{/* end - Filter and create button */}
        <Loading active={loading} height="300px">
          <div className="justify-content-center">
            <ProjectList searchBar={true} listProjects={listProjects} />
						<button type="button" className={`btn btn-primary loadBtn ${btnHiddenClass}`}
                    onClick={() => this.loadProjects(this.state.loadBtnCount)}>
							{loadingBtn &&
	              <Fragment>
	             		<span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>&nbsp;
	              </Fragment>
	            }
							{intl.formatMessage({ id: "general.load" })}
						</button>
          </div>
        </Loading>
      </div>
    );
  }
}
export default injectIntl(ProjectsPage);
ProjectsPage.contextType = UserContext;