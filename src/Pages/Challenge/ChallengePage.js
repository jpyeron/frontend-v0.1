import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import ChallengeCreate from "Components/Challenges/ChallengeCreate";
import ChallengeDetails from "Components/Challenges/ChallengeDetails";
import ChallengeEdit from "Components/Challenges/ChallengeEdit";

export default class ChallengePage extends Component {

  constructor(props){
    super(props);
    this.state = {
      defaultURL: "/challenge",
    };
  }

  render(){
    return (
      <div>
        <Switch>
          <Route path={this.state.defaultURL + "/create"} exact={true}
                component={ChallengeCreate} />
          <Route path={this.state.defaultURL + "/:id" } exact={true}
                component={ChallengeDetails} />
          <Route path={this.state.defaultURL + "/:id/edit"} exact={true}
                component={ChallengeEdit} />
        </Switch>
      </div>
    );
  }

}
