import React from 'react';
import { connectStats } from 'react-instantsearch-dom';
import { formatNumber } from 'Components/Tools/utils/utils.js'
import { FormattedMessage }  from "react-intl";

const SaveFiltersMobile = ({ nbHits, onClick }) => (
  <button className="button button-primary" onClick={onClick}>
		<FormattedMessage id="users.see" defaultMessage="see" />
		&nbsp;{formatNumber(nbHits)}&nbsp;
		<FormattedMessage id="users.members" defaultMessage="members" />
  </button>
);

export default connectStats(SaveFiltersMobile);
