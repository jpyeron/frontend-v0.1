import React, { Component } from "react";
import { injectIntl }  from "react-intl";
import "./legal.scss";

class EthicsPledge extends Component {

  render(){
		var {intl} = this.props;
    return (
    	<div className="container-fluid legal">
				<h1>{intl.formatMessage({id:'ethics.title'})}</h1>
		    <h2>{intl.formatMessage({id:'ethics.aware.title'})}</h2>
		    <h4>{intl.formatMessage({id:'ethics.aware.1'})}</h4>
		    <p>{ intl.formatMessage({id:'ethics.aware.1.text'})}</p>
		    <h4>{intl.formatMessage({id:'ethics.aware.2'})}</h4>
		    <p>{ intl.formatMessage({id:'ethics.aware.2.text'})}</p>
		    <h4>{intl.formatMessage({id:'ethics.aware.3'})}</h4>
		    <p>{ intl.formatMessage({id:'ethics.aware.3.text'})}</p>
		    <h4>{intl.formatMessage({id:'ethics.aware.4'})}</h4>
		    <p>{ intl.formatMessage({id:'ethics.aware.4.text'})}</p>
		    <h4>{intl.formatMessage({id:'ethics.aware.5'})}</h4>
		    <p>{ intl.formatMessage({id:'ethics.aware.5.text'})}</p>
		    <h2>{intl.formatMessage({id:'ethics.pledge.title'})}</h2>
		    <p>{ intl.formatMessage({id:'ethics.pledge.engage'})}</p>
		    <h5>{intl.formatMessage({id:'ethics.pledge.1'})}</h5>
		    <p>{ intl.formatMessage({id:'ethics.pledge.1.text'})}</p>
		    <h5>{intl.formatMessage({id:'ethics.pledge.2'})}</h5>
		    <p>{ intl.formatMessage({id:'ethics.pledge.2.text'})}</p>
		    <h5>{intl.formatMessage({id:'ethics.pledge.3'})}</h5>
		    <p>{ intl.formatMessage({id:'ethics.pledge.3.text'})}</p>
		    <h5>{intl.formatMessage({id:'ethics.pledge.4'})}</h5>
		    <p>{ intl.formatMessage({id:'ethics.pledge.4.text'})}</p>
		    <h5>{intl.formatMessage({id:'ethics.pledge.5'})}</h5>
		    <p>{ intl.formatMessage({id:'ethics.pledge.5.text'})}</p>
		    <h5>{intl.formatMessage({id:'ethics.pledge.6'})}</h5>
		    <p>{ intl.formatMessage({id:'ethics.pledge.6.text'})}</p>
		    <h5>{intl.formatMessage({id:'ethics.pledge.7'})}</h5>
		    <p>{ intl.formatMessage({id:'ethics.pledge.7.text'})}</p>
		    <h5>{intl.formatMessage({id:'ethics.pledge.8'})}</h5>
		    <p>{ intl.formatMessage({id:'ethics.pledge.8.text'})}</p>
		    <h5>{intl.formatMessage({id:'ethics.pledge.9'})}</h5>
		    <p>{ intl.formatMessage({id:'ethics.pledge.9.text'})}</p>
      </div>
    );
  }

}
export default injectIntl(EthicsPledge)