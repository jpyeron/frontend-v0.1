import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import defaultImg from "assets/img/default/default-project.jpg";
import BtnFollow from "../Tools/BtnFollow";
import BtnJoin from "../Tools/BtnJoin";
import InfoInterestsComponent from "Components/Tools/Info/InfoInterestsComponent";
import InfoSkillsComponent from "Components/Tools/Info/InfoSkillsComponent";
import BtnClap from "../Tools/BtnClap";
import { renderStatsModal, renderOwnerNames } from 'Components/Tools/utils/utils.js'

export default class ProjectHeader extends Component {

  editBtn(){
    if (this.props.project.is_admin) {
      return(
        <Link to={"/project/" + this.props.project.id + "/edit"}>
          <i className="fa fa-edit"/>
          <FormattedMessage id="entity.form.btnAdmin" defaultMessage="Edit" />
        </Link>
      )
    }
	}

	participatingToChallenges(challenges) {
		var userLang = navigator.language || navigator.userLanguage; 
		const lang = (localStorage.getItem("language") && localStorage.getItem("language") === "fr") || (!localStorage.getItem("language") && (userLang === "fr-FR" || userLang === "fr")) ? "fr" : "en"
		return (
			<p className="card-by"><FormattedMessage id="project.info.participatingToChallenges" defaultMessage="by " />
				{challenges.map((challenge, index, challenges) => { // filter to get only owners, and then map
					const rowLen = challenges.length
					return (
						<Fragment key={index}>
							<Link to={"/challenge/" + (challenge.id)}>
								{lang === "fr" && challenge.title_fr ? challenge.title_fr : challenge.title}
							</Link>
							{/* add comma, except for last item*/}
							{rowLen !== index + 1 && <span>, </span>}
						</Fragment>
					);
				})}
			</p>
		);
	}

  render(){
		var {banner_url, follower_count, has_followed, id, is_member, is_owner, members_count, short_description, short_title, title, interests, skills, has_clapped, claps_count, users, challenges, creator} = this.props.project;
    if(follower_count === undefined) follower_count = 0;
    if(members_count === undefined) members_count = 0;
    if(banner_url === "" || banner_url === undefined || banner_url === null){
      banner_url = defaultImg;
		}
    const bannerStyle = {
      backgroundImage: "url(" + banner_url + ")"
		};
    return(
      <div className="row projectHeader">
        <div className="col-12 title">
					<h1>{title}</h1>
          {this.editBtn()}
        </div>
        <div className="col-lg-7 col-md-12 projectHeader--banner">
          <div style={bannerStyle}></div>
        </div>
        <div className="col-lg-5 col-md-12 projectHeader--info">
					<p className="infos">#{short_title}</p>
					{/* <p className="infos">{is_private}</p> */}
          <p className="info">{short_description}</p>
					{users !== undefined && users.length > 0 && renderOwnerNames(users, creator)}
					{challenges[0] && this.participatingToChallenges(challenges)}
					<InfoInterestsComponent place="entity_header" title="" content={interests} displayCount="4"/>
          <InfoSkillsComponent place="entity_header" title="" content={skills} />
          <div className="projectStats">
						<span className="text" data-toggle="modal" data-target="#followersModal">
							<strong>{follower_count}</strong>&nbsp;
							<FormattedMessage id="user.profile.followers" defaultMessage="Followers" />{follower_count > 1 ? "s" : ""}
						</span>
            <span className="text" data-toggle="modal" data-target="#entityMembersModal">
							<strong>{members_count}</strong>&nbsp;
							<FormattedMessage id="entity.info.members" defaultMessage="Member" />{members_count > 1 ? "s" : ""}
						</span>
          </div>
          <div className="zoneBtnActions">
            <BtnFollow  followState={has_followed}
                        itemType="projects" itemId={id}
                        textFollow={<FormattedMessage id="project.info.btnFollow" defaultMessage="Follow project" />}
                        textUnfollow={<FormattedMessage id="project.info.btnUnfollow" defaultMessage="Unfollow project" />} />
            {!is_owner &&
              <BtnJoin  joinState={is_member}
                        itemType="projects" itemId={id}
                        textJoin={<FormattedMessage id="project.info.btnJoin" defaultMessage="Join project" />}
                        textUnjoin={<FormattedMessage id="project.info.btnUnjoin" defaultMessage="Leave project" />} />
						}
						<BtnClap itemType="projects" itemId={id} clapState={has_clapped} clapCount={claps_count}/>
          </div>
        </div>
				{follower_count ? renderStatsModal("followersModal", "projects", this.props.project) : "" /* trigers popup only if there are projects */}
				{renderStatsModal("entityMembersModal", "projects", this.props.project)}
      </div>
    );
  }
}
