import React, { Component } from "react";
import ListComponent from "../Tools/ListComponent";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";

export default class ProjectList extends Component {

  constructor(props){
    super(props);
    this.state = {
      listProjects: this.props.listProjects,
      searchBar: this.props.searchBar,
    };
  }

  static get defaultProps() {
    return {
      listProjects: [],
      searchBar: true,
      colMax: 3,
      displayHome: undefined,
      cardFormat: undefined,
    }
  }

  render(){
    const {colMax, listProjects, displayHome, cardFormat} = this.props
    return (
      <div className="projectsList">
        {displayHome && <h3><FormattedMessage id="projects.latest" defaultMessage="Latest projects" /></h3>}
        <ListComponent itemType="project" data={listProjects} colMax={colMax} cardFormat={cardFormat}/>
				{displayHome && <Link to="/projects" className="viewMoreRecents"><FormattedMessage id="home.viewMoreRecent" defaultMessage="View more" /></Link>}
      </div>
    );
  }
}
