import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import TitleInfo from "Components/Tools/TitleInfo";
import ReactQuill, { Quill } from 'react-quill';
import { ImageDrop } from 'quill-image-drop-module'
import ImageResize from 'quill-image-resize-module-react';
import quillEmoji from 'quill-emoji';
// import MagicUrl from 'quill-magic-url';
import BlotFormatter from 'quill-blot-formatter';
import "quill-emoji/dist/quill-emoji.css";
import 'react-quill/dist/quill.snow.css';
import "./FormTextAreaComponent.scss";

Quill.register('modules/imageDrop', ImageDrop)
Quill.register('modules/imageResize', ImageResize)
Quill.register({ // emojis support
  "formats/emoji": quillEmoji.EmojiBlot,
  'modules/emoji-shortname': quillEmoji.ShortNameEmoji,
  'modules/emoji-toolbar': quillEmoji.ToolbarEmoji,
  'modules/emoji-textarea': quillEmoji.TextAreaEmoji
}, true);
// Quill.register('modules/magicUrl', MagicUrl);
Quill.register('modules/blotFormatter', BlotFormatter);

export default class FormWysiwygComponent extends Component {

  constructor(props){
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  modules = {
    toolbar: [
      [{ 'header': 1 }, { 'header': 2 }],
      ['bold', 'italic', 'underline', 'strike'],
      [{ 'color': [] }, { 'background': [] }],
      [{ 'align': [] }],
      [{ 'script': 'sub'}, { 'script': 'super' }],
      ['code-block', 'blockquote'],
      [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
      ['link', 'image', 'video'],
      ['clean'],
			['emoji']
		],
    'emoji-toolbar': true,
    "emoji-textarea": true,
    "emoji-shortname": true,
		imageDrop: true,
    blotFormatter: {},
		// imageResize: {
		// 	displayStyles: {
		// 		backgroundColor: 'black',
		// 		border: 'none',
		// 		color: 'white'
		// 	},
		// 	modules: [ 'Resize', 'DisplaySize', 'Toolbar' ]
		// },
    // magicUrl: true
  };

  formats = [
    'header', 'align', 'color', 'background', 'script',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link', 'image', 'code-block', 'video', 'emoji'
  ];

  static get defaultProps() {
    return {
      id: "default",
      title: "Title",
			content: "",
			show: false,
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      placeholder: "",
      colSizeTitle: "col-12 col-sm-3",
      colSizeContent: "col-12 col-sm-9",
      row: 3,
      maxChar: undefined,
      mandatory: false,
      beHide: false
    }
  }

  handleChange(event){
    // console.log("EVENT : ", event);
    this.props.onChange(this.props.id, event);
  }

  renderBeHide(id, beHide){
    if(beHide){
      return (
        <div className="form-group form-check">
          <input type="checkbox" className="form-check-input" id={"show" + (id)} />
          <label className="form-check-label" htmlFor={"show" + (id)}>
            <FormattedMessage id="user.profile.edit.public" defaultMessage="Show to public" />
          </label>
        </div>
      );
    } else {
      return null;
    }
  }

  renderPrepend(prepend){
    if(prepend){
      return (
        <div className="input-group-prepend">
          <div className="input-group-text">{prepend}</div>
        </div>
      );
    } else {
      return null;
    }
  }

  render(){
    const {
			title,
			id,
			content,
			show,
      placeholder,
      colSizeTitle,
      colSizeContent,
      mandatory } = this.props;

    return(
      <div className="form-row formTextArea">
        <TitleInfo title={title} classSize={colSizeTitle} mandatory={mandatory}/>
        <div className={colSizeContent}>
					{!show && // only show button if prop is true
						<a className="btn btn-primary" data-toggle="collapse" href={`#${id}`} role="button" aria-expanded="false" aria-controls="collapseExample">
							<FormattedMessage id="entity.form.btnAdmin" defaultMessage="Edit" />
						</a>
					}
					<div className={show ? "" : "collapse"} id={id}>
	          <ReactQuill formats={this.formats}
												modules={this.modules}
	                      onChange={this.handleChange}
												placeholder={placeholder}
	                      style={{width: "100%", paddingBottom: "20px"}}
	                      theme="snow"
	                      value={content === null ? "" : content}/>
					</div>
        </div>
      </div>
    );
  }
}
