import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import TitleInfo from "Components/Tools/TitleInfo";
import InfoMaxCharComponent from "Components/Tools/Info/InfoMaxCharComponent";
import "./FormTextAreaComponent.scss";


export default class FormTextAreaComponent extends Component {

  static get defaultProps() {
    return {
      beHide: false,
      colSizeTitle: "col-12 col-sm-3",
      colSizeContent: "col-12 col-sm-9",
      content: "",
      errorCodeMessage: "",
      id: "default",
      isValid: undefined,
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      placeholder: "",
      maxChar: undefined,
      mandatory: false,
      row: 3,
      title: "Title",
    }
  }

  handleChange(event){
    this.props.onChange(event.target.id, event.target.value);
  }

  renderBeHide(id, beHide){
    if(beHide){
      return (
        <div className="form-group form-check">
          <input type="checkbox" className="form-check-input" id={"show" + (id)} />
          <label className="form-check-label" htmlFor={"show" + (id)}>
            <FormattedMessage id="user.profile.edit.public" defaultMessage="Show to public" />
          </label>
        </div>
      );
    } else {
      return null;
    }
  }

  renderPrepend(prepend){
    if(prepend){
      return (
        <div className="input-group-prepend">
          <div className="input-group-text">{prepend}</div>
        </div>
      );
    } else {
      return null;
    }
  }

  render(){
    const {
      beHide,
      colSizeContent,
      colSizeTitle,
      content,
      errorCodeMessage,
      id,
      isValid,
      maxChar,
      mandatory,
      placeholder,
      prepend,
      row,
      title,
      } = this.props;

    return(
      <div className="form-row formTextArea">
        <TitleInfo title={title} classSize={colSizeTitle} mandatory={mandatory}/>
        <div className={(colSizeContent) + " content"}>
          <div className="input-group">
            {this.renderPrepend(prepend)}
            <textarea className={"form-control " + (isValid !== undefined ? isValid ? "is-valid" : "is-invalid" : "")}
                      id={id}
                      placeholder={placeholder}
                      rows={row}
                      value={content === null ? "" : content}
                      onChange={this.handleChange.bind(this)} />
            {errorCodeMessage &&
              <div className="invalid-feedback">
                <FormattedMessage id={errorCodeMessage} defaultMessage="Value is not valid" />
              </div>
            }
          </div>
          <InfoMaxCharComponent content={content} maxChar={maxChar} />
          {this.renderBeHide(id, beHide)}
        </div>
      </div>
    );
  }
}
