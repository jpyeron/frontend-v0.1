import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import TitleInfo from "Components/Tools/TitleInfo";
import { applyPattern } from "Components/Tools/Forms/FormChecker";
import "./FormDefaultComponent.scss";


export default class FormDefaultComponent extends Component {

  static get defaultProps() {
    return {
      beHide: false,
      colSizeContent: "col-12 col-sm-9",
      colSizeTitle: "col-12 col-sm-3",
      content: "",
      errorCodeMessage: "",
      id: "default",
      isValid: undefined,
      mandatory: false,
      maxChar: undefined,
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      pattern: undefined,
      placeholder: "",
      title: "Title",
      type: "text",
    }
  }

  handleChange(event){
    var { id, value } = event.target;
    const { content, pattern } = this.props;
    value = applyPattern(value, content, pattern);
    this.props.onChange(id, value);
  }

  renderBeHide(id, beHide){
    if(beHide){
      return (
        <div className="form-group form-check">
          <input type="checkbox" className="form-check-input" id={"show" + (id)} pattern="" />
          <label className="form-check-label" htmlFor={"show" + (id)}>
            <FormattedMessage id="user.profile.edit.public" defaultMessage="Show to public" />
          </label>
        </div>
      );
    } else {
      return null;
    }
  }

  renderPrepend(prepend){
    if(prepend){
      return (
        <div className="input-group-prepend">
          <div className="input-group-text">{prepend}</div>
        </div>
      );
    } else {
      return null;
    }
	}

  render(){
    const {
      colSizeTitle,
      colSizeContent,
      content,
      errorCodeMessage,
      id,
      isValid,
      mandatory,
      placeholder,
      prepend,
      title,
			type } = this.props;
			
    return(
      <div className="form-row formDefault">
        <TitleInfo title={title} classSize={colSizeTitle} mandatory={mandatory}/>
        <div className={(colSizeContent) + " content"}>
          <div className="input-group">
            {this.renderPrepend(prepend)}
            <input  type={type}
                    className={"form-control " + (isValid !== undefined ? isValid ? "is-valid" : "is-invalid" : "")}
                    id={id}
                    placeholder={placeholder}
                    value={content === null ? "" : content}
                    onChange={this.handleChange.bind(this)} />
            {errorCodeMessage &&
              <div className="invalid-feedback">
                <FormattedMessage id={errorCodeMessage} defaultMessage="Value is not valid" />
              </div>
            }
          </div>

          {/* this.renderBeHide(id, beHide) */}
        </div>
      </div>
    );
  }
}
