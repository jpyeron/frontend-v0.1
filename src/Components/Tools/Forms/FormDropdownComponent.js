import React, { Component } from "react";
import { FormattedMessage, injectIntl }  from "react-intl";
import "./FormDefaultComponent.scss";
import TitleInfo from "Components/Tools/TitleInfo";
import $ from 'jquery';

class FormDropdownComponent extends Component {

  constructor(props){
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      id: "default",
      title: "Title",
      content: "",
      list: [],
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      colSizeTitle: "col-12 col-sm-3",
      colSizeContent: "col-12 col-sm-9",
      mandatory: false,
      beHide: false
    }
  }

  handleChange(event){
    this.props.onChange(this.props.id, event.target.value);
  }
	
	componentDidMount() {
		if(this.props.content) {
			// select entity current category
			$('#category').val(this.props.content);
		}
	}

  renderBeHide(id, beHide){
    if(beHide){
      return (
        <div className="form-group form-check">
          <input type="checkbox" className="form-check-input" id={"show" + (id)} />
          <label className="form-check-label" htmlFor={"show" + (id)}>
            <FormattedMessage id="user.profile.edit.public" defaultMessage="Show to public" />
          </label>
        </div>
      );
    } else {
      return null;
    }
  }

  render(){
    const {
      id,
      title,
      content,
      colSizeTitle,
      colSizeContent,
      list,
      mandatory,
			beHide,
			warningMsgId,
			intl } = this.props;

    return(
      <div className="form-row formDefault">
        <TitleInfo title={title} classSize={colSizeTitle} mandatory={mandatory}/>
        <div className={(colSizeContent) + " content"}>
					{warningMsgId && content === "draft" && <p className="dropdownWarningMsg">{intl.formatMessage({id:warningMsgId})}</p>}
          <div className="form-group">
            <select className="form-control" id="category"
                    value={content === null ? "" : content}
                    onChange={this.handleChange}>
              {list.map((status, index) => {
                return(
                  // <option key={index}>{status}</option>
                  <option value={status} key={index}>{intl.formatMessage({id:`entity.info.status.${status}`})}</option>
                );
              })}
            </select>
          </div>
          {this.renderBeHide(id, beHide)}
        </div>
      </div>
    );
  }
}
export default injectIntl(FormDropdownComponent)