import React, { Component, Fragment } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import Api from 'Api';
import "./BtnUploadFile.scss";

class BtnUploadFile extends Component {
  constructor(props){
    super(props);
    this.state = {
      uploading: false,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
			fileTypes: [""],
      itemId: "",
      itemType: "",
      maxSizeFile: 31457280,
      multiple: true,
      onChange: (result) => console.log("Oops, the function onChange is missing !", result),
      setListFiles: (listFiles) => console.log("Oops, the function setListFiles is missing !", listFiles),
      type: "",
      text: <FormattedMessage id="info-1000" defaultMessage="Attach file" />,
      textUploading: <FormattedMessage id="info-1001" defaultMessage="Uploading..." />,
      uploadNow: false,
    }
  }

  handleChange(event){
		event.preventDefault();
		const { intl, itemType } = this.props;
    // console.log(event.target.files);
    if(event.target.files) {
      if(!this.validFilesSize(event.target.files)){
        this.props.onChange({
          error: (<Fragment>
                    <FormattedMessage id="err-1001" defaultMessage="File's size too large" />
                    &nbsp;(max : {this.props.maxSizeFile / 1024 / 1024}Mo)
                  </Fragment>),
          url: ""
        });
      } else if(!this.props.uploadNow){
        // console.log("Documents à remonter : ", event.target.files)
				this.props.setListFiles(event.target.files);
				if(itemType === "needs") alert(intl.formatMessage({ id: "need.docs.uploaded.onCreate" }))
        document.getElementById("btnUpload").value = "";
      } else {
        // console.log("Launch local upload");
        this.uploadFiles(event.target.files);
      }
    }
  }



  uploadFiles(files){
    const { intl, itemId, itemType, multiple, type } = this.props;
    if(!itemId || !itemType || !type || !files){
      // console.log("Il manque un paramètre pour l'upload : ");
      // console.log("- itemId : " + itemId);
      // console.log("- itemType : " + itemType);
      // console.log("- type : " + type);
      this.props.onChange({error: <FormattedMessage id="err-" defaultMessage="An error has occured" />, url: ""});
    } else {
      this.setState({uploading: true});

      var bodyFormData = new FormData();
      if(multiple){
        Array.from(files).forEach((file) => {
          // console.log("file : ", file);
          bodyFormData.append(type + "[]", file);
        });
      } else {
        bodyFormData.append(type, files[0]);
      }

      var config = {
        headers: {'Content-Type': 'multipart/form-data'}
      };

      Api.post("/api/" + itemType + "/" + itemId + "/" + type, bodyFormData, config)
      .then((res) => {
        if (res.status === 200) {
          // console.log("Image uploaded !");
					this.setState({ uploading: false });
					if(itemType === "needs") alert(intl.formatMessage({ id: "need.docs.uploaded.onUpdate" }))
          if(res.data.url){
            this.props.onChange({error: "", url: res.data.url});
          } else if(this.props.refresh !== undefined){
            this.props.refresh();
          }
        } else {
          // console.log("An error has occured");
          this.setState({ uploading: false });
          this.props.onChange({error: <FormattedMessage id="err-" defaultMessage="An error has occured" />, url: ""});
        }
      })
      .catch(error => {
        // console.log(error);
        // console.log(error.response.data);
        this.setState({ uploading: false });
        this.props.onChange({error: error.response.data.status + " : " + error.response.data.error, url: ""});
      });
    }
  }

  validFilesSize(files) {
    const maxSizeFile = this.props.maxSizeFile;
    var nbFilesValid = 0;
    // console.log("Size file : ", files);
    if(files){
      for(var index = 0; index < files.length; index++){
        // console.log("Size file : " + files[index].size + " " + maxSizeFile);
        if(files[index].size <= maxSizeFile) {
          // console.log("Size accepted");
          nbFilesValid++;
        }
      }
    }
    // console.log(nbFilesValid, files.length);
    if(nbFilesValid === files.length) {
      return true;
    } else {
      return false;
    }
	}

	validFilesType(files) {
    const fileTypes = this.props.fileTypes;
    var nbFilesValid = 0;
    // console.log(files);
    if(files){
      for(var index = 0; index < files.length; index++){
        for(var i = 0; i < fileTypes.length; i++) {
          // console.log("Type : " + files[index].type + " ; list type check : " + fileTypes[i]);
          if(files[index].type === fileTypes[i]) {
            // console.log("Type accepted");
            nbFilesValid++;
            break;
          }
        }
      }
    }
    // console.log(nbFilesValid, files.length);
    if(nbFilesValid === files.length) {
      return true;
    } else {
      return false;
    }
  }

  render(){
    const { multiple, text, textUploading, fileTypes } = this.props;
    const { uploading } = this.state;

    if(uploading){
      return (
        <div className="upload-btn-wrapper">
          <div className="btn" disabled>
            <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
            &nbsp;
            {textUploading}
          </div>
					<input  type="file"
									accept={fileTypes.join()}
                  multiple={multiple ? true : false}
                  onChange={this.handleChange} disabled />
        </div>
      );
    } else {
      return(
        <div className="upload-btn-wrapper">
          <div className="btn">
            <i className="far fa-file"></i>
            {text}
          </div>
          <input  id="btnUpload"
									type="file"
									accept={fileTypes.join()}
                  multiple={multiple ? true : false}
                  onChange={this.handleChange} />
        </div>
      );
    }
  }
}
export default injectIntl(BtnUploadFile);