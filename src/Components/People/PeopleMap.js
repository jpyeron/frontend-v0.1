import React, { Component } from "react";
import Map from "Components/Map/Map";
import "./PeopleMap.scss";

export default class PeopleMap extends Component {
  render(){
    return (
      <div className="row peopleMap">
        <div className="col-12">
          <div className="map">
            <Map />
          </div>
        </div>
      </div>
    );
  }
}
