import React, { Component, Fragment,} from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import { UserContext } from 'UserProvider';
import BtnFollow from "Components/Tools/BtnFollow";
import BtnJoin from "Components/Tools/BtnJoin";
import NeedDelete from "./NeedDelete";
import NeedWorkers from "./NeedWorkers";
import Feed from "Components/Feed/Feed";
import NeedDocsManagement from "./NeedDocsManagement";
import InfoSkillsComponent from "Components/Tools/Info/InfoSkillsComponent";
import Api from "Api";
import { linkify } from 'Components/Tools/utils/utils.js'
import "./NeedCard.scss";

export default class NeedCard extends Component {

  constructor(props){
    super(props);
    this.state = {
      posts: [],
    }
  }

  static get defaultProps() {
		return {
      changeMode: () => console.log("Missing function"),
      need: undefined,
			refresh: () => console.log("Missing function"),
			mode: ""
    }
  }

  changeDisplayFeed(){
    this.setState({seeAllFeed: !this.state.seeAllFeed});
  }
  
  getDaysLeft(end_date){
    const now = new Date();
    const end = new Date(end_date);
    var daysLeft = Math.round((end - now) / 1000 / 60 / 60 / 24);
    if(daysLeft < 0){
      daysLeft = 0;
    }
    return daysLeft;
	}
	
	toggleMode() { // change mode/view of card, depending on its current state
		var {mode, changeMode} = this.props
		mode == "details" && changeMode("card")
		mode == "card" && changeMode("details")
	}

  getFeedApi() {
    Api.get("/api/feeds/" + this.props.need.feed_id).then(res=>{
			this.setState({posts: res.data});
    }).catch(error=>{
      // console.log(error);
    });
	}

  componentDidMount(){
    this.getFeedApi()
  }
	
  render(){
		const { need, changeMode, refresh, mode } = this.props;
		const { posts, seeAllFeed } = this.state;
		const contentWithLinks = linkify(need.content)
		var posts_count = posts.length
		var comments_count = 0
		posts.map((post) => ( // map through all posts of need and count all comments
			comments_count += post.comments.length
		))
    if(need === undefined) {
			return null;
		} else  {
			if(this.props.cardFormat !== "compact") {
				return (
					<div className="needCard">
            <div className="needCard--header d-flex justify-content-between">
							<div className="d-block">
	              <h5>{need.title}</h5>
                {need.is_owner &&
                  <span className="needOwnerAction">
                    <i className="fa fa-edit" onClick={() => changeMode("update")} />
                    <NeedDelete need={need} refresh={refresh}/>
                  </span>
                }
								<div className="needProject">
									<FormattedMessage id="needsPage.project" defaultMessage="Project:" />
									<Link to={"/project/"+need.project.id}>
										{need.project.title}
									</Link>
								</div>
							</div>
							{(need.is_urgent || need.status === "completed") &&
								<div className={`statusTag ${need.is_urgent} ${need.status} justify-content`}>
									{need.status === "completed" && <FormattedMessage id="need.completed" defaultMessage="Complited" />}
									{need.is_urgent && need.status !== "completed" && <FormattedMessage id="need.urgent" defaultMessage="Urgent" />}
								</div>
							}
            </div>

						<div className="needCard--content">
							{mode == "card" &&
		            <div className="needContent">
		              {need.content.substring(0, 100)} {need.content.length > 100 && "..."}
		            </div>
							}
							{mode == "details" && <div className="needContent" dangerouslySetInnerHTML={{ __html: contentWithLinks }} />}
							{mode == "card" && <InfoSkillsComponent content={need.skills} place="entity_header" limit="3"/>}
							{mode == "details" && 
								<Fragment>
									<InfoSkillsComponent content={need.skills} place="entity_header"/>
									<NeedWorkers need={need} mode="card" />
									<NeedDocsManagement need={need} mode="details" />
									<div className="needFeed">
			              <h6><FormattedMessage id="need.feed.title" defaultMessage="Discussion and results" /></h6>
			              <div className="zoneFeed" style={!seeAllFeed ? {height: "350px"} : {}}>
			                  <Feed displayCreate={this.context.connected ? true : false } 
			                        feedId={need.feed_id} isAdmin={need.is_admin} />
			              </div>
			              <button className="btn btn-primary btnSee" onClick={() => this.changeDisplayFeed()}>
			                {seeAllFeed ?
			                  <FormattedMessage id="need.feed.seeLess" defaultMessage="See Less" />
			                :
			                  <FormattedMessage id="need.feed.seeMore" defaultMessage="See More" />
			                }
			              </button>
			            </div>
								</Fragment>
							}
						</div>

						<div className="needCard--footer">

							{mode == "card" && <div className="needStats" onClick={() => this.toggleMode()}>
								{posts_count} <FormattedMessage id="post.posts" defaultMessage="posts" />{posts_count > 1 ? "s" : ""}
								&nbsp;/ {comments_count} <FormattedMessage id="post.comments" defaultMessage="Comments" />{comments_count > 1 ? "s" : ""}
							</div>}
							<div className="d-flex justify-content-between">
								{!need.is_owner &&
									<div className="interaction">
		                <BtnJoin  itemId={need.id}
		                          itemType="needs" 
		                          joinState={need.is_member}
		                          textJoin={<FormattedMessage id="need.help.willHelp" defaultMessage="I'll help" />}
		                          textUnjoin={<FormattedMessage id="need.help.stopHelp" defaultMessage="I can't help" />} />
			              <BtnFollow  followState={need.has_followed}
			                          itemType="needs" itemId={need.id} />
									</div>
	              }
		            <button className="btn btn-outline-primary btnToggleMode" onClick={() => this.toggleMode()}>
									{mode == "card" && <FormattedMessage id="need.card.seeMore" defaultMessage="See More" />}
									{mode == "details" && <FormattedMessage id="need.card.seeLess" defaultMessage="See Less" />}
		            </button>
							</div>
            </div>
	        </div>
				);
			}
			else return (
				<div className="needCard row">
          <div className="col-12 cardHeader">
            <div className="d-flex">
              <span className="needTitle">
								<Link to={"/project/"+need.project.id+"#needs"}>
                	<h5>{need.title}</h5>
								</Link>
              </span>
            </div>
            <div className="needContent">
              {need.content.substring(0, 100)}
              {need.content.length > 100 && "..."}
            </div>
            <div className="needProject">
							<FormattedMessage id="needsPage.project" defaultMessage="Project:" />
							<Link to={"/project/"+need.project.id}>
								{need.project.title}
							</Link>
            </div>
          </div>
        </div>
      );
		}
	}
}
NeedCard.contextType = UserContext;