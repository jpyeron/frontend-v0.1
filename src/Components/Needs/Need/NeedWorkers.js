import React, { Component, Fragment} from "react";
import { FormattedMessage } from "react-intl";
import defaultLogoImg from "assets/img/default/default-user.png";
import "./NeedWorkers.scss";
import NeedWorkersDelete from "./NeedWorkersDelete";

export default class NeedWorkers extends Component {


  static get defaultProps() {
		return {
      need: undefined,
      refresh: () => console.log("Missing function"),
    }
  }
  
  getDaysLeft(end_date){
    const now = new Date();
    const end = new Date(end_date);
    var daysLeft = Math.round((end - now) / 1000 / 60 / 60 / 24);
    if(daysLeft < 0){
      daysLeft = 0;
    }
    return daysLeft;
  }

  render(){
    const { need, mode, refresh } = this.props;
    if(need === undefined || need.creator === undefined) {
			return null;
		} else  {
			var CreatorLogoUrl = !need.creator.logo_url ? defaultLogoImg : need.creator.logo_url
			return (
        <div className="needWorkers">
          <FormattedMessage id="need.card.working" defaultMessage="Working on it: " />
          <span className="workers">
            <span className="imgList">
              <a href={"/user/" + need.creator.id}>
                <img  className="peopleImg"
                      src={CreatorLogoUrl}
                      alt={need.creator.first_name + " " + need.creator.last_name}/>
              </a>
              {need.users.map((user, index) => {
                if(index < 6) {
                  var logoUrl = !user.logo_url ? defaultLogoImg : user.logo_url
                  return (
                    <Fragment key={index}>
                      <a href={"/user/" + user.id} >
                        <img  className="peopleImg"
                              src={logoUrl}
                              alt={user.first_name + " " + user.last_name}/>
                      </a>
                      {mode === "update" && need.is_owner &&
                        <NeedWorkersDelete worker={user} itemId={need.id} itemType="needs" refresh={refresh} />
                      }
                  </Fragment>
                  );
                } else if(index === 6) {
                  return (
                    <div className="moreMembers">+{need.users.length - 6}</div>
                  );
                }
                return "";
              })}
            </span>
          </span>
        </div>
      );
		}
  }
}
