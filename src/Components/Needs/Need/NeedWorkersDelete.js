import React, { Component} from "react";
import { injectIntl }  from "react-intl";
import Api from 'Api';

class NeedWorkersDelete extends Component {

  constructor(props){
    super(props);
    this.state = {
      sending: false,
    }
  }

  static get defaultProps() {
		return {
      itemId: undefined,
      itemType: undefined,
      worker: undefined,
    }
  }
  
  deleteWorker(worker){
    const { itemId, itemType, intl } = this.props;
		// console.log("worker", worker);
    this.setState({sending: true, error: ""}); 

    Api.delete("/api/" + itemType + "/" + itemId + "/members/" + worker.id)
    .then(res=>{
      // console.log(res);
      this.setState({sending: false});
			alert(intl.formatMessage({id:"need.user.deleted"}))
    })
    .catch(error=>{
			// console.log(error);
			alert(intl.formatMessage({id:"need.user.deleted"}))
			// this.props.refresh();
      this.setState({ sending: false });
    });
  }

  render(){
    const { worker } = this.props;
    const { sending } = this.state;
    if(worker === undefined) {
			return null;
		} else  {
			return (
        <button type="button"
                className="close delete" 
                aria-label="Close"
                disabled={sending}
                onClick={() => this.deleteWorker(worker)}>
          {sending ?
            <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
          :
            <span aria-hidden="true">&times;</span>
          }
        </button>
      );
		}
  }
}
export default injectIntl(NeedWorkersDelete)