function findMentions(content){
  if(content){
    var regexMentions = /\(\(.*?:.*?:.*?\)\)/g
    const match = content.match(regexMentions);
    if(match) {
      var mentions = match.map((mention) => {
        mention = mention.substring(2, mention.length - 2);
        var type = mention.split(":")[0] === "@" ? "user" : "project";
        const id = mention.split(":")[1];
        const content = mention.split(":")[2];
        return {"obj_type": type, "obj_id": id, "obj_match": type + content}
      });
      return mentions;
    } else {
      return [];
    }
  } else {
    return [];
  }
}

function noHTMLMentions(content){
  if(content){
    var regexMentions = /<a href=.*?>.*?<\/a>/g
    const match = content.match(regexMentions);
    // console.log("Match : ", match);
    if(match) {
      match.forEach((mention) => {
				var mentionObj =   mention.substring(10, mention.length - 4);
				var mentionId =    mentionObj.split("\"")[0].split("/")[1];
				var mentionType =  mentionObj.split(">")[1].substring(0, 1);
				var mentionValue = mentionObj.split(">")[1].substring(1, mention.length);
				var formatReactMention = "((" + mentionType + ":" + mentionId + ":" + mentionValue + "))";
        // console.log("Mention : ", formatReactMention);
				content = content.replace(mention, formatReactMention)
      });
    }
  }
  return content;
}

function transformMentions(content){
  if(content){
    var regexMentions = /\(\(.*?:.*?:.*?\)\)/g
    const match = content.match(regexMentions);
    if(match) {
      match.forEach((mention) => {
        var mention_obj = mention.substring(2, mention.length - 2)
				// var obj_type = mention_obj.split(":")[0][0] === "@" ? "user" : "project";
				var obj_type = mention_obj.split(":")[0] === "@" ? "user" : "project";
        content = content.replace(mention,
          "<a href=\"/" + obj_type + "/" + mention_obj.split(":")[1] + "\">" +
          mention_obj.split(":")[0] + mention_obj.split(":")[2] + "</a>");
          // mention_obj.split(":")[0][1] + mention_obj.split(":")[2] + "</a>");
      });
    }
  }
  return content.replace(/\([()]*(\([^()]*\)[^()]*)*\)/g, ""); // remove content inside double parenthesis. ex: ((content))
}

module.exports = { findMentions, noHTMLMentions, transformMentions }
