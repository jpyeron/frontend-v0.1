import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import CommentCreate from "./CommentCreate";
import CommentUpdate from "./CommentUpdate";
import PostDelete from "Components/Feed/Posts/PostDelete";
import defaultImg from "assets/img/default/default-user.png";
// import BtnClap from "Components/Tools/BtnClap";
import { linkify } from 'Components/Tools/utils/utils.js'
import "./CommentDisplay.scss";

export default class CommentDisplay extends Component {

	constructor(props){
		super(props);
		this.state = {
			showMoreComm: false,
			edit: false,
			commentKey: "",
		};
	}
	
  static get defaultProps() {
		return {
			comments: undefined,
			user: "",
			isAdmin: "false"
    }
  }

	showMoreComments(){
		this.setState({showMoreComm : !this.state.showMoreComm});
	}

  isEdit(commentKey) {
    this.setState({"edit": !this.state.edit});
		this.setState({"commentKey": commentKey});
  }

  render(){
		const { comments, postId, refresh, user } = this.props;
		comments.sort((a, b) => a.id - b.id); // sort comments by id, so have most recent on top
		var moment = require('moment');
    return (
      <div className="commentDisplay">
        <div className="actionBox">
          {comments &&
            <div className="commentList">
              {comments.map((comment, index) => {
								var userImg = comment.creator.logo_url ? comment.creator.logo_url : defaultImg;
                const userImgStyle = {backgroundImage: "url(" + userImg + ")"}
								const creator_id = comment.creator.id;
								const contentWithLinks = linkify(comment.content)
								var commentClassName;
								if(!this.state.showMoreComm) { // dynamically hide 5th comment and more
									commentClassName = index > 3 ? "comment hidden" : "comment"
								}
								else commentClassName = "comment"; // show all comments on "show more" click
								var postDate = (moment().format('X') - moment(comment.created_at).format('X') > 600000) ?
									moment(comment.created_at).calendar()
									:
									moment(comment.created_at).startOf('hour').fromNow()
								return (
									<Fragment key={index}>
										{/* Display a "show more" link when there are more than 4 comments, and when we still haven't clicked on it */}
										{index === 4 ? !this.state.showMoreComm && 
											<span className="showMore" onClick={this.showMoreComments.bind(this)}><FormattedMessage id="general.showmore" /></span>
										: ""}
										<div className={commentClassName}>
											<div className="topContent">
												<div className="topBar">
													<div className="left d-flex">
														<div className="userImgContainer">
														<Link to={"/user/" + creator_id}>
															<div className="userImg" style={userImgStyle}></div>
														</Link>
														</div>
														<div className="comment-content">
															<div className="d-flex justify-content-between">
																<div className="comment-author align-self-start">
																	<Link to={"/user/" + creator_id}>
																		{comment.creator.first_name + " " + comment.creator.last_name}
																	</Link>
																</div>
																<div className="comment-manage right align-self-end d-flex flex-row">
																	{(user.id === creator_id || this.props.isAdmin) &&
																	<div className="btn-group dropright">
																		<button type="button" className="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																			•••
																		</button>
																		<div className="dropdown-menu">
																			{/* {user.id === creator_id && */}
																				<Fragment>
																				{index === this.state.commentKey ?
																					!this.state.edit ?
																						<div onClick={this.isEdit.bind(this, index)}><i className="fa fa-edit postUpdate"/> <FormattedMessage id="feed.object.update" defaultMessage="Update" /></div>
																					:
																						<div onClick={this.isEdit.bind(this, index)}>
																							<FormattedMessage id="feed.object.cancel" defaultMessage="Cancel" />
																						</div>
																				:
																				<div onClick={this.isEdit.bind(this, index)}><i className="fa fa-edit postUpdate"/> <FormattedMessage id="feed.object.update" defaultMessage="Update" /></div>
																				}
																				<PostDelete postId={postId} commentId={comment.id} type="comment" refresh={refresh}/>
																			</Fragment>
																			{/* } */}
																			{/* <div onClick={console.log("report")}><i className="fa fa-flag postFlag"/> <FormattedMessage id="feed.object.report" defaultMessage="Report" /></div> */}
																		</div>
																	</div>
																	}
																</div>
															</div>
															{this.state.edit && index === this.state.commentKey ?
																<CommentUpdate postId={postId} commentId={comment.id} content={comment.content} isEdit={this.isEdit.bind(this, index)} refresh={refresh} user={user}/>
															:
																<div className="text extra" dangerouslySetInnerHTML={{ __html: contentWithLinks }} />
															}
														</div>
													</div>
												</div>
												<div className="comment-actions d-flex flex-row">
													<div className="comment-date">{postDate}</div>
													{/* <BtnClap itemType="comments" itemId={comment.id} type="text" clapState={comment.has_clapped} refresh={refresh}/> */}
												</div>
											</div>
										</div>
									</Fragment>
                );
              })}
            </div>
					}
					<CommentCreate postId={postId} refresh={refresh} user={user}/>
        </div>
      </div>
    );
  }
}
