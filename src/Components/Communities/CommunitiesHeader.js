import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import "../Main/Similar.scss";

export default class CommunitiesHeader extends Component {

  render(){
    return (
      <div className="communitiesHeader">
        <h1><FormattedMessage id="communities.header.title" defaultMessage="Explore communities" /></h1>
        <p><FormattedMessage id="communities.header.description" defaultMessage="Faites connaître vos actions & besoins, et communiquez avec des communautés inspirantes." /></p>
        <hr/>
      </div>
    );
  }

}
