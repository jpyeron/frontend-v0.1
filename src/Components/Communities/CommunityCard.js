import React, { Component } from "react";
import { Link } from "react-router-dom";
import defaultImg from "assets/img/default/default-group.jpg";
// import InfoSkillsComponent from "Components/Tools/Info/InfoSkillsComponent";
import BtnClap from "../Tools/BtnClap";
import { renderOwnerNames, renderTeam } from 'Components/Tools/utils/utils.js'
import "../Main/Cards.scss";

export default class CommunityCard extends Component {

  static get defaultProps() {
    return {
      community : undefined,
    }
  }

  render(){
    if(this.props.community !== undefined){
      var { id, title, banner_url, short_description, creator, users, has_clapped, claps_count, skills } = this.props.community;
      // var communityOwnerName = creator !== undefined ? creator.first_name + " " + creator.last_name  : "";
      if(banner_url === undefined || banner_url === null || banner_url === ""){
        banner_url = defaultImg;
			}
			const communityImgStyle = {
				backgroundImage: 'url(' + banner_url + ')',
			}

      if(this.props.cardFormat !== "compact") {
        return (
	        <div className="card cardCommunity" key={id}>
						<Link to={"/community/" + (id)}>
							<div style={communityImgStyle} className="communityImg" />
						</Link>
						<BtnClap itemType="communities" itemId={id} clapState={has_clapped} clapCount={claps_count}/>
						<div className="card-content">
							<Link to={"/community/" + (id)}>
								<h5 className="card-title">{title}</h5>
							</Link>
							<p className="card-desc">{short_description}</p>
							{users !== undefined && users.length > 0 && renderOwnerNames(users, creator)}
							{/* <InfoSkillsComponent place="entity_header" limit="3" content={skills} /> */}
						</div>
						<div className="cardfooter">
							<hr/>
							{users !== undefined && users.length > 0 && renderTeam(users, "community", id)}
						</div>
					</div>
        );
      }
      else {
        return (
          <div className="card cardCommunity cardCommunitySmall" key={id}>
          <Link to={"/community/" + (id)}>
            <div style={communityImgStyle} className="communityImg"/>
          </Link>
            <BtnClap itemType="communities" itemId={id} clapState={has_clapped}  clapCount={claps_count}/>
            <div className="card-content">
  						<Link to={"/community/" + (id)}>
  	            <h5 className="card-title">{title}</h5>
  						</Link>
              <p className="card-desc">{short_description}</p>
            </div>
          </div>
        );
      }
    } else {
      return null;
    }
  }
}
