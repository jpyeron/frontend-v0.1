import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Redirect } from "react-router-dom";
import { UserContext } from 'UserProvider';
import Api from "Api";
import CommunityHeader from "./CommunityHeader";
import Feed from "../Feed/Feed";
import InfoHtmlComponent from "Components/Tools/Info/InfoHtmlComponent";
import Loading from "Components/Tools/Loading";
import { stickyTabNav, scrollToActiveTab } from 'Components/Tools/utils/utils.js'
import Alert from "Components/Tools/Alert";
import $ from 'jquery';
import "Components/Main/Similar.scss";

export default class CommunityDetails extends Component {

  constructor(props){
    super(props);
    this.state = {
      community: this.props.community,
      failedLoad: false,
      loading: true,
    };
  }

  componentWillMount(){
    if(this.props.community){
      // console.log("COMMUNITY RECU");
      this.setState({loading: false});
    } else {
      Api.get("/api/communities/" + this.props.match.params.id )
      .then(res=>{
        // console.log("Community got : ", res.data);
        if(res.data){
          this.setState({community: res.data, loading: false});
        } else {
          this.setState({failedLoad: true, loading: false});
        }
      })
      .catch(error=>{
        // console.log("ERR : ", error);
        this.setState({failedLoad: true, loading: false});
      });
    }
  }

  render(){
    const { community, failedLoad, loading } = this.state;
		var urlParams = new URLSearchParams(window.location.search);
		var urlHash = new URLSearchParams(window.location.search);
		var url = new URL(window.location.href);
		var urlSuccessParam = url.searchParams.get("success");

		if(failedLoad){
      return( <Redirect to="/communities" /> );
		}		
		// make different tab active depending if user is member or not
		var newsTabClasses, aboutTabClasses, newsPaneClasses, aboutPaneClasses
		if (community) {
			if(community.is_member || urlParams.get('tab') == "feed") {
				newsTabClasses = "nav-item nav-link active";aboutTabClasses = "nav-item nav-link"
				newsPaneClasses = "tab-pane active";aboutPaneClasses = "tab-pane"
			}
			else {
				newsTabClasses = "nav-item nav-link";aboutTabClasses = "nav-item nav-link active"
				newsPaneClasses = "tab-pane";aboutPaneClasses = "tab-pane active"
			}
			setTimeout(function(){ 
				stickyTabNav()
				scrollToActiveTab()
				if(urlSuccessParam === "1") { // if url success param is 1, show "saved changes" success alert
					$(".alert-success").show() // display success message
					setTimeout(function(){ $(".alert-success").hide(300) }, 2000); // hide it after 2sec
				};
			}, 700); // had to add setTimeout for the function to work
		}
    return(
      <Loading active={loading}>
        {community &&
          <div className="communityDetails container-fluid">
            <CommunityHeader community={community} />
            <nav className="nav nav-tabs container-fluid">
							<a className={newsTabClasses} href="#news" data-toggle="tab"><FormattedMessage id="entity.tab.news" defaultMessage="News & Update" /></a>
              <a className={aboutTabClasses} href="#about" data-toggle="tab"><FormattedMessage id="community.tab.about" defaultMessage="About the Community" /></a>
            </nav>
            <div className="tabContainer">
              <div className="tab-content justify-content-center container-fluid">
								<div className={newsPaneClasses} id="news">
								{community.feed_id && 
									<Feed feedId={community.feed_id}
												// display post create component to everyone on public communities, and only to members on private communities (connected users only)
												displayCreate={this.context.connected ? !community.is_private ? true : community.is_member ? true : false : false}
												isAdmin={community.is_admin}
									/>}
                </div>
                <div className={aboutPaneClasses} id="about">
                  <InfoHtmlComponent title="" content={community.description} />
                </div>
              </div>
            </div>
						<Alert type="success" message={<FormattedMessage id="general.editSuccessMsg" defaultMessage="The changes have been saved successfully." />} />
          </div>
        }
      </Loading>
    );
  }
}
CommunityDetails.contextType = UserContext;
