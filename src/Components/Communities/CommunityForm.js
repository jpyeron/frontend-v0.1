import React, { Component, Fragment } from "react";
import { FormattedMessage, injectIntl }  from "react-intl";
import { Link } from "react-router-dom";
// import MembersList from "../Members/MembersList";
/*** Form objects ***/
import FormChipsComponent from "Components/Tools/Forms/FormChipsComponent";
import FormDefaultComponent from "Components/Tools/Forms/FormDefaultComponent";
import FormImgComponent from "Components/Tools/Forms/FormImgComponent";
import FormInterestsComponent from "Components/Tools/Forms/FormInterestsComponent";
import FormSkillsComponent from "Components/Tools/Forms/FormSkillsComponent";
import FormTextAreaComponent from "Components/Tools/Forms/FormTextAreaComponent";
import FormToggleComponent from "Components/Tools/Forms/FormToggleComponent";
import FormWysiwygComponent from "Components/Tools/Forms/FormWysiwygComponent";
/*** Validators ***/
import FormValidator from "Components/Tools/Forms/FormValidator";
import communityFormRules from "./communityFormRules";
/*** Images/Style ***/
import DefaultImg from "assets/img/default/default-group.jpg";
import "./CommunityForm.scss";

class CommunityForm extends Component {

  validator = new FormValidator(communityFormRules);

  static get defaultProps() {
    return {
      mode: "create",
      community: {
        "banner_url": "",
        "description": "",
        "interests": [],
        "public": true,
        "short_description": "",
        "short_title": "",
        "skills": [],
        "title": "",
				"is_private": true, // force projects to be private (TEMP @TOFIX)
      },
      sending: false,
    }
  }

  handleChange(key, content){
    /* Validators start */
    const state =  {};
    state[key] = content;
    const validation = this.validator.validate(state);
    if(validation[key] !== undefined){
      const stateValidation = {};
      stateValidation["valid_"+ key] = validation[key];
      this.setState(stateValidation);
    }
    /* Validators end */
    this.props.handleChange(key, content);
  }

  handleSubmit(event){
    event.preventDefault();
		/* Validators control before submit */
		var firsterror = true;
    const validation = this.validator.validate(this.props.community);
    if(validation.isValid) {
      this.props.handleSubmit();
    } else {
      const stateValidation = {};
      Object.keys(validation).forEach(key => {
        if(key !== "isValid"){
					if(validation[key].isInvalid && firsterror) { // if field is invalid and it's the first field that has error
						var element = document.querySelector(`#${key}`); // get element that is not valid
						const y = element.getBoundingClientRect().top + window.pageYOffset - 140; // calculate it's top value and remove 25 of offset
						window.scrollTo({top: y, behavior: 'smooth'}); // scroll to element to show error
						firsterror = false // set to false so that it won't scroll to second invalid field and further
					}
          stateValidation["valid_"+ key] = validation[key];
        }
      });
      this.setState(stateValidation);
    }
  }

  disableBtn(){
    var {title, short_title} = this.props.community;
    if (title.trim() === "" || short_title.trim() === "") {
      return(true)
    }else {
      return(false)
    }
  }

  renderBtnsForm(){
    const { community, mode, sending } = this.props;
    var urlBack = "/communities";
    var textAction = "Create";
    if(mode === "edit") {
      urlBack = "/community/" + community.id;
      textAction = "Update";
    }

    return(
      <div className="row communityFormBtns">
          <Link to={urlBack}>
            <button type="button"
                    className="btn btn-outline-primary">
              <FormattedMessage id="entity.form.btnCancel" defaultMessage="Cancel" />
            </button>
          </Link>
          <button type="submit"
                  className="btn btn-primary"
                  style={{marginRight: "10px"}}
                  disabled={this.disableBtn() || sending}>
            {sending &&
              <Fragment>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
                &nbsp;
              </Fragment>
            }
            <FormattedMessage id={"entity.form.btn" + textAction} defaultMessage={textAction} />
          </button>
        </div>
    );
  }

  render(){
    const { valid_title, 
            valid_short_title,
            valid_short_description,
            valid_interests,
            valid_skills } = this.state ? this.state : "";
    var { community, mode, intl } = this.props;

    return(
      <form onSubmit={this.handleSubmit.bind(this)} className="communityForm">
        <FormDefaultComponent
          content={community.title}
          errorCodeMessage={valid_title ? valid_title.message : ""}
          id="title"
          isValid={valid_title ? !valid_title.isInvalid : undefined}
          onChange={this.handleChange.bind(this)}
          mandatory={true}
          title={intl.formatMessage({id:'entity.info.title'})}
          placeholder={intl.formatMessage({id:'community.form.title.placeholder'})} />
        {/* {mode === "create" && */}
        <FormDefaultComponent
          content={community.short_title}
          errorCodeMessage={valid_short_title ? valid_short_title.message : ""}
          id="short_title"
          isValid={valid_short_title ? !valid_short_title.isInvalid : undefined}
          mandatory={true}
          onChange={this.handleChange.bind(this)}
          pattern={/[A-Za-z0-9]/g}
          title={intl.formatMessage({id:'entity.info.short_name'})}
          prepend="#"
          placeholder={intl.formatMessage({id:'community.form.short_title.placeholder'})} />
        {/* } */}
        <FormTextAreaComponent
          content={community.short_description}
          errorCodeMessage={valid_short_description ? valid_short_description.message : ""}
          id="short_description"
          isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
          mandatory={true}
          maxChar={140}
          onChange={this.handleChange.bind(this)}
          title={intl.formatMessage({id:'entity.info.short_description'})}
          row={3}
          placeholder={intl.formatMessage({id:'community.form.short_description.placeholder'})} />
        {mode === "edit" &&
          <FormWysiwygComponent
						id="description"
            title={intl.formatMessage({id:'entity.info.description'})}
            placeholder={intl.formatMessage({id:'community.form.description.placeholder'})}
						content={community.description}
						show={true}
            onChange={this.handleChange.bind(this)} />
        }
        {mode === "edit" &&
          <FormImgComponent
            type="banner"
            id="banner_url"
            imageUrl={community.banner_url}
            itemId={community.id}
            itemType= "communities"
            title={intl.formatMessage({id:'community.info.banner_url'})}
            content={community.banner_url}
            defaultImg={DefaultImg}
            onChange={this.handleChange.bind(this)} />
        }
        {mode === "edit" &&
          <FormChipsComponent
            id="hashtags"
            title={intl.formatMessage({id:'entity.info.hashtags'})}
            placeholder={intl.formatMessage({id:'general.hashtags.placeholder'})}
            content={community.hashtags}
            onChange={this.handleChange.bind(this)}
            maxChips={5}
            prepend="#" />
        }
        <FormInterestsComponent
          content={community.interests}
          errorCodeMessage={valid_interests ? valid_interests.message : ""}
          id="interests"
          isValid={valid_interests ? !valid_interests.isInvalid : undefined}
          mandatory={true}
          onChange={this.handleChange.bind(this)}
          title={intl.formatMessage({id:'entity.info.interests'})} />
        <FormSkillsComponent
          content={community.skills}
          errorCodeMessage={valid_skills ? valid_skills.message : ""}
					id="skills"
					type="group"
          isValid={valid_skills ? !valid_skills.isInvalid : undefined}
          mandatory={true}
          onChange={this.handleChange.bind(this)}
          title={intl.formatMessage({id:'entity.info.skills'})}
          placeholder={intl.formatMessage({id:'general.skills.placeholder'})} />
				{mode === "edit" &&
					<FormToggleComponent
						id="is_private"
						warningMsgId="community.info.publicPrivateToggleMsg"
	          title={intl.formatMessage({id:'entity.info.is_private'})}
	          choice1={<FormattedMessage id="general.public" defaultMessage="Public" />}
	          choice2={<FormattedMessage id="general.private" defaultMessage="Private" />}
	          colorChoice1="#27B40E"
	          colorChoice2="#F9530B"
	          isChecked={community.is_private}
	          onChange={this.handleChange.bind(this)}
	          />
				}
        {this.renderBtnsForm()}
      </form>
    );
  }
}
export default injectIntl(CommunityForm)