import React, { Component, Fragment } from "react";
import { Link, Redirect } from "react-router-dom";
import { FormattedMessage, injectIntl } from "react-intl";
import { UserContext } from "UserProvider";
import Alert from "Components/Tools/Alert";
import Logo from "assets/img/logo.svg";
import Api from 'Api';
import $ from 'jquery';
import "./Auth.scss";

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      error: "",
      fireRedirect: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
	}

	resendVerifEmail() {
		var param = {
			"user": {
				"email": this.state.email
			}
		}
		Api.post('/api/users/resend_confirmation', param)
      .then((res) => {
				$(".mail-sent.alert").show() // display confirmation message
				setTimeout(function(){ $(".mail-sent.alert").hide(700) }, 2400);
      })
      .catch(error => {
				// console.log(error.response);
			});
	}

  handleSubmit(event) {
    event.preventDefault();
    let userContext = this.context;
    userContext
      .signIn(this.state.email, this.state.password)
      .then(() => {
        this.setState({ errors: "", fireRedirect: true });
      })
      .catch(errors => {
        this.setState({
          error: errors[0],
        });
        // alert(this.state.error);
      });
  }

  render() {
    var returnUrl = this.props.location.currentUrl
    ? this.props.location.currentUrl
    : "/";
    const { error } = this.state;
    var errorMessage = error.includes("Invalid login") ? ( // if error is about invalid login, show this message (translated)
			<FormattedMessage id="err-4010" />
    ) : error.includes("A confirmation email was sent") ? ( // if error is that account has not been validated, show this message (translated) + button to resend mail
			<Fragment>
				<p><FormattedMessage id="signIn.resendConf" /></p>
				<button className="btn btn-primary" onClick={() => this.resendVerifEmail()}>
					<FormattedMessage id="signIn.resendConf.btn" />
				</button>
				<div className={`mail-sent alert alert-success`} role="alert">
					<FormattedMessage id="signIn.resendConf.msgSent" defaultMessage="Message sent" />
				</div>
			</Fragment>
		) : (
			error
		);
    let userContext = this.context;
    const { intl } = this.props;

    if (userContext.connected) {
      this.setState({ fireRedirect: true });
    }
    if (this.state.fireRedirect) {
      this.setState({ fireRedirect: false });
      //Control SDG & Skills
      if (
        userContext.user.skills.length === 0 ||
        userContext.user.interests.length === 0
      ) {
        return <Redirect push to={"/complete-profile"} />;
      } else {
        return <Redirect push to={returnUrl} />;
      }
    }
    return (
      <>
        <div className="auth-form row align-items-center signIn">
          <div className="col-12 col-lg-5 leftPannel d-flex align-items-center justify-content-center">
            <Link to="/">
              <img src={Logo} className="logo" alt="JoGL icon" />
            </Link>
          </div>
          <div className="col-12 col-lg-7 rightPannel">
            <div className="form-content">
              <div className="form-header">
                <h2 className="form-title" id="signModalLabel">
                  <FormattedMessage id="signIn.title" defaultMessage="Sign in" />
                </h2>
                <p>
                  <FormattedMessage
                    id="signIn.description"
                    defaultMessage="Enter your details below."
                  />
                </p>
              </div>
              <div className="form-body">
                <form onSubmit={this.handleSubmit}>
                  <div className="form-group">
                    <label className="form-check-label" htmlFor="email">
                      <FormattedMessage
                        id="signIn.email"
                        defaultMessage="Email"
                      />
                    </label>
                    <input
                      type="email"
                      name="email"
                      id="email"
                      className="form-control"
                      placeholder={intl.formatMessage({
                        id: "signIn.email.placeholder"
                      })}
                      onChange={this.handleChange.bind(this)}
                    />
                  </div>
                  <div className="form-group">
                    <div className="row rowPwd">
                      <div className="col-5">
                        <label className="form-check-label" htmlFor="password">
                          <FormattedMessage
                            id="signIn.pwd"
                            defaultMessage="Password"
                          />
                        </label>
                      </div>
                      <div className="col-7 text-right forgotPwd">
                        <Link
                          to="/auth/forgot-password"
                          className="nav-link"
                          tabIndex="-1"
                        >
                          <FormattedMessage
                            id="signIn.forgotPwd"
                            defaultMessage="Forgot your password ?"
                          />
                        </Link>
                      </div>
                    </div>
                    <input
                      type="password"
                      name="password"
                      id="password"
                      className="form-control"
                      placeholder={intl.formatMessage({
                        id: "signIn.pwd.placeholder"
                      })}
                      onChange={this.handleChange.bind(this)}
                    />
                  </div>
                  {error !== "" && <Alert type="danger" message={errorMessage} />}

                  <div className="goToSignUp">
                    <span>
                      <FormattedMessage
                        id="signIn.newJoin"
                        defaultMessage="Don’t have an account?"
                      />
                    </span>
                    <span className="goToSignUp--signup">
                      <Link to="/signup" className="nav-link">
                        <FormattedMessage
                          id="header.signUp"
                          defaultMessage="Sign up"
                        />
                      </Link>
                    </span>
                  </div>

                  <button
                    type="submit"
                    className="btn btn-primary btn-block"
                    color="primary"
                  >
                    <FormattedMessage
                      id="signIn.btnSignIn"
                      defaultMessage="Sign in"
                    />
                  </button>
                  {/* <div className="form-group form-check remember">
                    <input type="checkbox" className="form-check-input" id="rememberMe" />
                    <label className="form-check-label" htmlFor="rememberMe">
                      <FormattedMessage id="signIn.remember" defaultMessage="Remember me" />
                    </label>
                  </div> */}
                </form>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default injectIntl(SignIn);
SignIn.contextType = UserContext;
