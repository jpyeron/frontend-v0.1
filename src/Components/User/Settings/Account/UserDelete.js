import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { Redirect } from "react-router-dom";
import { UserContext } from 'UserProvider';
import Api from 'Api';
import $ from 'jquery';
import Alert from 'Components/Tools/Alert';
import "./UserDelete.scss";


export default class UserDelete extends Component {

  constructor(props) {
    super(props);
    this.state = {
      accountDelete: false,
      errors: ""
    };
    this.handleClickYes = this.handleClickYes.bind(this);
  };

  handleClickYes(event){
    event.preventDefault();
    let userContext = this.context;
    const headers = {
			'access-token': localStorage.getItem('access-token'),
			'client': localStorage.getItem('client'),
			'uid': localStorage.getItem('uid')
		}
    Api.delete('/api/users/' + localStorage.getItem("userId"), {headers: headers})
      .then(res => {
        // console.log("archive ok");
        userContext.logout();
        //close
				$('#closeDeleteModal').click();
        this.setState({accountDelete: true});
      })
      .catch(error => {
        // console.log(error);
        this.setState({ errors : error.toString() });
      })
  };

  renderDeleteModal(){
    const { errors } = this.state;
    var errorMessage = errors.includes("err-") ? <FormattedMessage id={errors} defaultMessage="An error has occured" /> : errors ;

    return (
      <div className="modal fade" id="deleteAccountModal" tabIndex="-1" role="dialog" aria-labelledby="deleteAccountModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="deleteAccountModalLabel">
                <FormattedMessage id="settings.account.delete.modal.title" defaultMessage="Delete my JoGL account" />
              </h5>
              <button type="button" id="closeDeleteModal" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {errors !== "" && <Alert type="danger" message={errorMessage} /> }
              <p className="deleteModalMessage">
                <FormattedMessage id="settings.account.delete.modal.message"
                                defaultMessage="Are you sure to delete your JoGL ?*" />
                <br/>
                <span className="deleteModalInfo">
                  <FormattedMessage id="settings.account.delete.modal.info"
                                    defaultMessage="(* : The account is kept 30 days before a permanent deletion)" />
                </span>
              </p>
              <div className="btnGroup">
                <button type="button" className="btn btn-outline-danger" onClick={this.handleClickYes}>
                  <FormattedMessage id="general.yes" defaultMessage="Yes" />
                </button>
                <button type="button" className="btn btn-success" data-dismiss="modal">
                  <FormattedMessage id="general.no" defaultMessage="No" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    if (this.state.accountDelete) {
			return <Redirect to="/" />
		} else {
      return (
        <div>
          <button type="button" className="btn btn-outline-danger deleteActBtn" data-toggle="modal" data-target="#deleteAccountModal">
            <FormattedMessage id="settings.account.delete.btn" defaultMessage="Delete my JoGL account" />
          </button>
          {this.renderDeleteModal()}
        </div>
      );
    }
  }
}
UserDelete.contextType = UserContext;
