import React, { Component, Fragment } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { Redirect } from "react-router-dom";
import Api from "Api";
import Feed from "Components/Feed/Feed";
import Loading from "Components/Tools/Loading";
import CommunityList from "Components/Communities/CommunityList";
import InfoAddressComponent from "Components/Tools/Info/InfoAddressComponent";
import InfoDefaultComponent from "Components/Tools/Info/InfoDefaultComponent";
import InfoInterestsComponent from "Components/Tools/Info/InfoInterestsComponent";
import InfoSkillsComponent from "Components/Tools/Info/InfoSkillsComponent"
import ProjectList from "Components/Projects/ProjectList";
import UserHeader from "./UserHeader";
import Alert from "Components/Tools/Alert";
import $ from 'jquery';
import { UserContext } from "UserProvider";
import { Helmet } from "react-helmet";
import { linkify } from 'Components/Tools/utils/utils.js'
import { scrollToActiveTab, stickyTabNav } from 'Components/Tools/utils/utils.js'
import "./UserProfile.scss";

class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      failedLoad: false,
      loading: true,
      user: this.props.user,
      listProjects: [],
      listCommunities: [],
    };
  }

  static get defaultProps() {
    return {
      user: {
        id: null, email: "", first_name: "", last_name: "", nickname: "",age: "",
        category: "", affiliation: "", country: "", city: "", address: "", phone_number: "",
        bio: "", short_bio: "",sign_in_count: 0, confirmed_at: "", interests: [], skills: []
      }
    };
  }

  componentDidMount() {
    const userId = this.props.match.params.id;
    this.getProfile(userId);
  }

  componentWillReceiveProps(nextProps) {
    // console.log("willReceiveProps");
    const userId = nextProps.match.params.id;
    this.getProfile(userId);
	}

  getProfile(userId) {
    this.setState({ loading: true });
    Api.get("/api/users/" + userId)
      .then(res => {
        // console.log("User got : ", res.data);
        this.setState({ loading: false, user: res.data, userLoaded: true });
      })
      .catch(error => {
        // console.log("ERR : ", error);
        this.setState({ failedLoad: true });
      });
  }

  loadProjects() {
    const userId = this.props.match.params.id;
    //Api.get("/api/projects/mine")
    Api.get("/api/users/" + userId + "/projects")
		.then(res => {
			var listProjects = [];
			// console.log("res au dessu", res);
			res.data.map(projects => {
				return listProjects.push(projects);
			});
			this.setState({ listProjects: listProjects });
		})
		.catch(error => {
			// console.log(error);
		});
  }
	
  loadCommunity() {
		const userId = this.props.match.params.id;
    Api.get("/api/users/" + userId + "/communities")
		.then(res => {
			var listCommunities = [];
			// console.log("res au dessu", res);
			res.data.map(community => {
				return listCommunities.push(community);
			});
			this.setState({ listCommunities: listCommunities });
		})
		.catch(error => {
			// console.log(error);
		});
  }

  render() {
    var listProjects = this.state.listProjects;
    var listCommunities = this.state.listCommunities;

    if (this.state.failedLoad) {
      return <Redirect to="/people" />;
    }
		const user = this.state.user;
		var url = new URL(window.location.href);
		var urlSuccessParam = url.searchParams.get("success");
		var { intl } = this.props;
		
		setTimeout(function(){ 
			// scrollToActiveTab() // if there is a hash in the url and the tab exists, click and scroll to the tab
			stickyTabNav("isEdit") // make the tab navigation bar sticky on top when we reach its scroll position
			if(urlSuccessParam === "1") { // if url success param is 1, show "saved changes" success alert	
				$(".alert-success").show() // display success message
				setTimeout(function(){ $(".alert-success").hide(300) }, 2000); // hide it after 2sec
			};
		}, 700); // had to add setTimeout for the function to work


    var bioWithLinks = user.bio ? linkify(user.bio) : "";
    var user_category;
    // @TODO - to improve, just temporary fix
    var isFrench = localStorage.getItem("language") === "fr";
    if (user.category === "fulltime_worker")
      user_category = isFrench
        ? "Travailleu.r.se à plein temps"
        : "Fulltime worker";
    if (user.category === "parttime_worker")
      user_category = isFrench
        ? "Travailleu.r.se à temps partiel"
        : "Parttime worker";
    if (user.category === "fulltime_student")
      user_category = isFrench
        ? "Étudiant.e à plein temps"
        : "Fulltime student";
    if (user.category === "parttime_student")
      user_category = isFrench
        ? "Étudiant.e à temps partiel"
        : "Parttime student";
    if (user.category === "retired")
      user_category = isFrench ? "Retraité.e" : "Retired";
    if (user.category === "between_jobs")
      user_category = isFrench ? "Entre deux emplois" : "Between jobs";
    if (user.category === "forprofit")
      user_category = isFrench
        ? "Organisation à but lucratif"
        : "For Profit Organisation";
    if (user.category === "nonprofit")
      user_category = isFrench
        ? "Organisation à but non lucratif"
        : "Non Profit Organisation";

    // make different tab active depending if user is member or not (@TODO to improve)
    var newsTabClasses, aboutTabClasses, newsPaneClasses, aboutPaneClasses;
    if (user && this.context.connected) {
      if (!user.has_followed && user.id !== this.context.user.id) {
        newsTabClasses = "nav-item nav-link";
        aboutTabClasses = "nav-item nav-link active";
        newsPaneClasses = "tab-pane";
        aboutPaneClasses = "tab-pane active";
      } else {
        newsTabClasses = "nav-item nav-link active";
        aboutTabClasses = "nav-item nav-link";
        newsPaneClasses = "tab-pane active";
        aboutPaneClasses = "tab-pane";
      }
    } else {
      newsTabClasses = "nav-item nav-link";
      aboutTabClasses = "nav-item nav-link active";
      newsPaneClasses = "tab-pane";
      aboutPaneClasses = "tab-pane active";
    }
    // Dynamic SEO meta tags
    var SEO_title = user.first_name + " " + user.last_name + " | JOGL";
    var SEO_desc = user.bio;

    return (
      <div className="userProfile">
        <Helmet>
          <title>{SEO_title}</title>
          <meta name="title" content={SEO_title} data-react-helmet="true" />
          <meta name="description" content={SEO_desc} data-react-helmet="true" />
          <meta property="og:title" content={SEO_title} data-react-helmet="true" />
          <meta property="og:image" content={user.logo_url} data-react-helmet="true" />
        </Helmet>
        <Loading active={this.state.loading}>
          <div className="userHeader justify-content-center container-fluid">
            <UserHeader user={this.state.user} />
          </div>
					<nav className="nav nav-tabs container-fluid">
            <a className={newsTabClasses} href="#news" data-toggle="tab">
              <FormattedMessage id="user.profile.tab.feed" defaultMessage="Feed" />
            </a>
            <a className={aboutTabClasses} href="#about" data-toggle="tab">
              <FormattedMessage id="user.profile.tab.about" defaultMessage="About" />
            </a>
            <a className="nav-item nav-link" href="#collections" data-toggle="tab" onClick={() => this.loadProjects()+this.loadCommunity()}>
              <FormattedMessage id="user.profile.tab.collections" defaultMessage="collection" />
            </a>
          </nav>
          <div className="tabContainer">
            <div className="tab-content justify-content-center container-fluid">
              <div className={newsPaneClasses} id="news">
                {user.feed_id && (
                  <Feed feedId={user.feed_id} displayCreate={ // show post box only to the user 
                      this.context.connected
                        ? user.id === this.context.user.id
                          ? true
                          : false
                        : false
                    }
                    isAdmin={user.is_admin} />
                )}
              </div>

              <div className={aboutPaneClasses} id="about">
                {user.bio === null &&
                user.address === null &&
                user.category === null &&
                user.affiliation === null &&
                user.interests.length === 0 &&
                user.age === null &&
                user.phone_number === null &&
                user.skills.length === 0 ? (
                  <p>{intl.formatMessage({ id: "general.noInfo" })}</p> // display message if user has no profile information available
                ) : (
                  <Fragment>
                    <InfoDefaultComponent
                      title={intl.formatMessage({ id: "user.profile.bio" })}
                      content={bioWithLinks}
                      containsHtml={true}
                    />
                    <InfoDefaultComponent
                      title={intl.formatMessage({id: "user.profile.category"})}
                      content={user_category}
                    />
                    <InfoDefaultComponent
                      title={intl.formatMessage({id: "user.profile.affiliation"})}
                      content={user.affiliation}
                    />
                    <InfoInterestsComponent
                      title={intl.formatMessage({id: "user.profile.interests"})}
                      content={user.interests}
                    />
                    <InfoAddressComponent
                      title={intl.formatMessage({ id: "user.profile.address" })}
                      address={user.address}
                      city={user.city}
                      country={user.country}
                    />
                    <InfoSkillsComponent
                      title={intl.formatMessage({ id: "user.profile.skills" })}
                      content={user.skills}
                      place="userProfile"
                    />
                  </Fragment>
                )}
              </div>

              <div className="tab-pane" id="collections">
                <h3>
                  <FormattedMessage id="user.profile.tab.projects" defaultMessage="Projects" />
                </h3>
                <ProjectList searchBar={false} listProjects={listProjects} />
                <h3>
                  <FormattedMessage id="user.profile.tab.communities" defaultMessage="Communities" />
                </h3>
                <CommunityList searchBar={false} listCommunities={listCommunities} />
              </div>
            </div>
          </div>
					<Alert type="success" message={<FormattedMessage id="general.editSuccessMsg" defaultMessage="The changes have been saved successfully." />} />
        </Loading>
      </div>
    );
  }
}
export default injectIntl(UserProfile);
UserProfile.contextType = UserContext;
