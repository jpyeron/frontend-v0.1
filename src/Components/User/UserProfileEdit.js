import React, { Component } from "react";
import { FormattedMessage, injectIntl }  from "react-intl";
import { Link, Redirect } from "react-router-dom";
import { toAlphaNum } from "Components/Tools/Nickname.js";
import Api from 'Api';
import Loading from "Components/Tools/Loading";
import { UserContext } from 'UserProvider';
/*** Form objects ***/
import FormDefaultComponent from "Components/Tools/Forms/FormDefaultComponent";
import FormTextAreaComponent from "Components/Tools/Forms/FormTextAreaComponent";
import FormImgComponent from "Components/Tools/Forms/FormImgComponent";
import FormInterestsComponent from "Components/Tools/Forms/FormInterestsComponent";
import FormSkillsComponent from "Components/Tools/Forms/FormSkillsComponent";
import FormToggleComponent from "../Tools/Forms/FormToggleComponent";
import FormSelectComponent from "Components/Tools/Forms/FormSelectComponent";
/*** Validators ***/
import FormValidator from "Components/Tools/Forms/FormValidator";
import userProfileFormRules from "./userProfileFormRules";
/*** Images/Style ***/
import DefaultImg from "assets/img/default/default-user.png";
import "./UserProfileEdit.scss";

class UserProfileEdit extends Component {

  validator = new FormValidator(userProfileFormRules);

  constructor(props) {
    super(props);
    this.state = {
      failedLoad: false,
      loading: true,
      user: this.props.user,
      userUpdated: false,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount(){
    if(!this.props.user){
			const userId = this.props.match.params.id;
      Api.get("/api/users/"+ userId).then(res=>{
        this.setState({user: res.data, loading: false});
      }).catch(error=>{
        console.log("ERR : ", error);
        this.setState({failedLoad: true, loading: false});
      })
    } else {
			this.setState({loading: false});
    }
  }

  handleChange(key, content){
		var newUser = this.state.user;
    newUser[key] = content;
    if(key === "nickname"){
      content = toAlphaNum(content);
    }
    /* Validators start */
    const state =  {};
    state[key] = content;
    const validation = this.validator.validate(state);
    if(validation[key] !== undefined){
      const stateValidation = {};
      stateValidation["valid_"+ key] = validation[key];
      this.setState(stateValidation);
    }
    /* Validators end */
    this.setState({ user: newUser });
  }

  handleSubmit(event){
    event.preventDefault();
    const user = this.state.user;
    /* Validators control before submit */
    const validation = this.validator.validate(user);
    if(validation.isValid) {
      Api.patch('/api/users/'+ user.id, {user})
      .then(res=>{
        // console.log(res);
        this.setState({userUpdated: true});
      })
      .catch(error=>{
        // console.log(error);
      })
    } else {
			var firsterror = true;
			const stateValidation = {};
      Object.keys(validation).forEach(key => { // map through all the object of validation
        if(key !== "isValid"){
					if(validation[key].isInvalid && firsterror) { // if field is invalid and it's the first field that has error
						var element = document.querySelector(`#${key}`); // get element that is not valid
						const y = element.getBoundingClientRect().top + window.pageYOffset - 100; // calculate it's top value and remove 25 of offset
						window.scrollTo({top: y, behavior: 'smooth'}); // scroll to element to show error
						firsterror = false // set to false so that it won't scroll to second invalid field and further
					}
          stateValidation["valid_"+ key] = validation[key];
        }
			});
      this.setState(stateValidation);
    }
  }

  render(){
    const { valid_first_name,
            valid_last_name,
            valid_nickname,
            valid_category,
            valid_affiliation,
            valid_interests,
            valid_skills } = this.state ? this.state : "";
		var { failedLoad, loading, user, userUpdated } = this.state;
		let userContext = this.context;
		// redirect to all members page, if fail load, or if user is not connected, or if it's not the connected users profile		
    if(failedLoad || !userContext.user || this.props.match.params.id != userContext.user.id) {
      return( <Redirect to="/people" /> );
    }

    if(userUpdated){
			window.location = `/user/${(user.id)}?success=1`;
      // return( <Redirect push to={"/user/" + (user.id)} /> );
		}
    var {intl} = this.props;

    return(
      <Loading active={loading}>
        {user ?
          <div className="userProfileEdit">
            <div className="container-fluid justify-content-center">
            <h1><FormattedMessage id="user.profile.edit.title" defaultMessage="Edit my Profile"/></h1>
              <form onSubmit={this.handleSubmit}>
								<FormDefaultComponent
									id="first_name"
									placeholder={intl.formatMessage({id:'user.profile.firstname.placeholder'})}
        					title={intl.formatMessage({id:'user.profile.firstname'})}
									content={user.first_name}
									onChange={this.handleChange.bind(this)}
									errorCodeMessage={valid_first_name ? valid_first_name.message : ""}
									isValid={valid_first_name ? !valid_first_name.isInvalid : undefined}
									mandatory={true} />
                <FormDefaultComponent
									id="last_name"
									placeholder={intl.formatMessage({id:'user.profile.lastname.placeholder'})}
      						title={intl.formatMessage({id:'user.profile.lastname'})}
                  content={user.last_name}
                  onChange={this.handleChange.bind(this)}
                  errorCodeMessage={valid_last_name ? valid_last_name.message : ""}
                  isValid={valid_last_name ? !valid_last_name.isInvalid : undefined}
                  mandatory={true} />
                <FormDefaultComponent
                  id="nickname"
                  placeholder={intl.formatMessage({id:'user.profile.nickname.placeholder'})}
      						title={intl.formatMessage({id:'user.profile.nickname'})}
                  content={user.nickname}
                  onChange={this.handleChange.bind(this)}
                  prepend="@"
                  mandatory={true}
                  errorCodeMessage={valid_nickname ? valid_nickname.message : ""}
                  isValid={valid_nickname ? !valid_nickname.isInvalid : undefined}
                  pattern={/[A-Za-z0-9]/g} />
								<FormDefaultComponent
									id="age"
									placeholder={intl.formatMessage({id:'user.profile.age.placeholder'})}
      						title={intl.formatMessage({id:'user.profile.age'})}
									content={user.age}
									onChange={this.handleChange.bind(this)} />
								{/* <FormDefaultComponent
									id="email"
									placeholder={intl.formatMessage({id:'signIn.email.placeholder'})}
      						title={intl.formatMessage({id:'signIn.email'})}
									content={user.email}
									onChange={this.handleChange.bind(this)} /> */}
								<FormSelectComponent
									id="category"
      						title={intl.formatMessage({id:'user.profile.category'})}
									content={user.category}
									mandatory={true}
									errorCodeMessage={valid_category ? valid_category.message : ""}
									isValid={valid_category ? !valid_category.isInvalid : undefined}
									onChange={this.handleChange.bind(this)} />
								<FormDefaultComponent
									id="affiliation"
									placeholder={intl.formatMessage({id:'user.profile.affiliation.placeholder'})}
      						title={intl.formatMessage({id:'user.profile.affiliation'})}
									content={user.affiliation}
									mandatory={true}
									errorCodeMessage={valid_affiliation ? valid_affiliation.message : ""}
									isValid={valid_affiliation ? !valid_affiliation.isInvalid : undefined}
									onChange={this.handleChange.bind(this)} />
								<FormDefaultComponent
									id="short_bio"
									placeholder={intl.formatMessage({id:'user.profile.bioShort.placeholder'})}
      						title={intl.formatMessage({id:'user.profile.bioShort'})}
									content={user.short_bio}
									onChange={this.handleChange.bind(this)}
									/>
								<FormTextAreaComponent
									id="bio"
									placeholder={intl.formatMessage({id:'user.profile.bio.placeholder'})}
      						title={intl.formatMessage({id:'user.profile.bio'})}
									content={user.bio}
									row={5}
									onChange={this.handleChange.bind(this)}
									/>
								<FormImgComponent
									itemId={user.id}
									fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
									maxSizeFile={4194304}
									itemType="users"
									type="avatar"
									id="logo_url"
      						title={intl.formatMessage({id:'user.profile.logo_url'})}
									imageUrl={user.logo_url}
									defaultImg={DefaultImg}
									onChange={this.handleChange.bind(this)} />
								<FormInterestsComponent
									id="interests"
      						title={intl.formatMessage({id:'user.profile.interests'})}
									content={user.interests}
									mandatory={true}
									errorCodeMessage={valid_interests ? valid_interests.message : ""}
									isValid={valid_interests ? !valid_interests.isInvalid : undefined}
									onChange={this.handleChange.bind(this)} />
								<FormSkillsComponent
									id="skills"
									type="user"
									placeholder={intl.formatMessage({id:'general.skills.placeholder'})}
      						title={intl.formatMessage({id:'user.profile.skills'})}
									content={user.skills}
									mandatory={true}
									errorCodeMessage={valid_skills ? valid_skills.message : ""}
									isValid={valid_skills ? !valid_skills.isInvalid : undefined}
									onChange={this.handleChange.bind(this)} />
								<FormDefaultComponent
									id="phone_number"
									placeholder={intl.formatMessage({id:'general.phone.placeholder'})}
      						title={intl.formatMessage({id:'general.phone'})}
									content={user.phone_number}
									onChange={this.handleChange.bind(this)} />
								<FormDefaultComponent
									id="address"
									placeholder={intl.formatMessage({id:'general.address.placeholder'})}
      						title={intl.formatMessage({id:'general.address'})}
									content={user.address}
									onChange={this.handleChange.bind(this)} />
								<FormDefaultComponent
									id="city"
									placeholder={intl.formatMessage({id:'general.city.placeholder'})}
      						title={intl.formatMessage({id:'general.city'})}
									content={user.city}
									onChange={this.handleChange.bind(this)} />
								<FormDefaultComponent
									id="country"
									placeholder={intl.formatMessage({id:'general.country.placeholder'})}
      						title={intl.formatMessage({id:'general.country'})}
									content={user.country}
									onChange={this.handleChange.bind(this)} />
								<FormToggleComponent
									id="can_contact"
									title={intl.formatMessage({id:'user.profile.canContact'})}
									choice1={<FormattedMessage id="general.no" defaultMessage="No" />}
									choice2={<FormattedMessage id="general.yes" defaultMessage="Yes" />}
									colorChoice1="#F9530B"
									colorChoice2="#27B40E"
									isChecked={user.can_contact !== false ? true : false}
									onChange={this.handleChange.bind(this)} />
								<FormToggleComponent
									id="mail_newsletter"
									title={intl.formatMessage({id:'signUp.mail_newsletter'})}
									choice1={<FormattedMessage id="general.no" defaultMessage="No" />}
									choice2={<FormattedMessage id="general.yes" defaultMessage="Yes" />}
									colorChoice1="#F9530B"
									colorChoice2="#27B40E"
									isChecked={user.mail_newsletter}
									onChange={this.handleChange.bind(this)} />
								<FormToggleComponent
									id="mail_weekly"
									title={intl.formatMessage({id:'signUp.mail_weekly'})}
									choice1={<FormattedMessage id="general.no" defaultMessage="No" />}
									choice2={<FormattedMessage id="general.yes" defaultMessage="Yes" />}
									colorChoice1="#F9530B"
									colorChoice2="#27B40E"
									isChecked={user.mail_weekly}
									onChange={this.handleChange.bind(this)} />
                <div className="buttons">
                  <Link to={"/user/" + (user.id)}>
                    <button type="button"
                            className="btn btn-outline-primary">
                      <FormattedMessage id="user.profile.edit.back" defaultMessage="Back" />
                    </button>
                  </Link>
                  <button className="btn btn-primary"
                          style={{marginRight: "10px"}}>
                    <FormattedMessage id="user.profile.edit.submit" defaultMessage="Update my profile" />
                  </button>
                </div>
              </form>
            </div>
          </div>
        :
          <div className="errorMessage text-center">
            Unable to load component
          </div>
        }
      </Loading>
    );
  }
}

export default injectIntl(UserProfileEdit)
UserProfileEdit.contextType = UserContext;