import React, { Component } from "react";
import { FormattedMessage, injectIntl }  from "react-intl";
import { Redirect } from "react-router-dom";
import { UserContext } from 'UserProvider';
import Api from "Api";
import ChallengeHeader from "./ChallengeHeader";
import DocumentsManager from "Components/Tools/Documents/DocumentsManager";
import Feed from "../Feed/Feed";
import InfoInterestsComponent from "Components/Tools/Info/InfoInterestsComponent";
import InfoSkillsComponent from "Components/Tools/Info/InfoSkillsComponent";
import InfoHtmlComponent from "Components/Tools/Info/InfoHtmlComponent";
import Loading from "Components/Tools/Loading";
import { stickyTabNav, scrollToActiveTab } from 'Components/Tools/utils/utils.js'
import "Components/Main/Similar.scss";

class ChallengeDetails extends Component {

  constructor(props){
    super(props);
    this.state = {
      challenge: this.props.challenge,
      failedLoad: false,
      loading: true,
    };
  }

  componentWillMount(){
    if(this.props.challenge){
      // console.log("CHALLENGE RECU");
      this.setState({loading: false});
    } else {
      Api.get("/api/challenges/" + this.props.match.params.id )
      .then(res=>{
        // console.log("Challenge got : ", res.data);
        if(res.data){
          this.setState({challenge: res.data, loading: false});
        } else {
          this.setState({failedLoad: true, loading: false});
        }
      })
      .catch(error=>{
        // console.log("ERR : ", error);
        this.setState({failedLoad: true, loading: false});
      });
    }
	}

  render(){
		const { challenge, failedLoad, loading } = this.state;
		var urlParams = new URLSearchParams(window.location.search);
		var {intl} = this.props;
		let userContext = this.context;
		var userLang = navigator.language || navigator.userLanguage; 
		const lang = (localStorage.getItem("language") && localStorage.getItem("language") === "fr") || (!localStorage.getItem("language") && (userLang === "fr-FR" || userLang === "fr")) ? "fr" : "en"
		// if fail has loaded or if status is draft & we are not admin, redirect to challenges page
    if(challenge && (failedLoad || (!challenge.is_admin && challenge.status === "draft"))){
      return( <Redirect to="/challenges" /> );
    }
		
		// make different tab active depending if user is member or not
		var newsTabClasses, aboutTabClasses, newsPaneClasses, aboutPaneClasses
		if (challenge) {
			if(challenge.is_member || urlParams.get('tab') == "feed") {
				newsTabClasses = "nav-item nav-link active";aboutTabClasses = "nav-item nav-link"
				newsPaneClasses = "tab-pane active";aboutPaneClasses = "tab-pane"
			}
			else {
				newsTabClasses = "nav-item nav-link";aboutTabClasses = "nav-item nav-link active"
				newsPaneClasses = "tab-pane";aboutPaneClasses = "tab-pane active"
			}
			setTimeout(function(){
				stickyTabNav() // make the tab navigation bar sticky on top when we reach its scroll position
				scrollToActiveTab() // if there is a hash in the url and the tab exists, click and scroll to the tab
			}, 700); // had to add setTimeout to make it work
		}
		
    return(
      <Loading active={loading}>
        {challenge &&
          <div className="challengeDetails container-fluid">
            <ChallengeHeader challenge={challenge} lang={lang}  />
            <nav className="nav nav-tabs container-fluid">
							<a className={newsTabClasses} href="#news" data-toggle="tab"><FormattedMessage id="entity.tab.news" defaultMessage="News & Update" /></a>
              <a className={aboutTabClasses} href="#about" data-toggle="tab"><FormattedMessage id="challenge.tab.about" defaultMessage="About the Challenge" /></a>
              {/* <a className="nav-item nav-link" href="#howto" data-toggle="tab"><FormattedMessage id="entity.tab.howTo" defaultMessage="How to Participate ?" /></a> */}
              {/* <a className="nav-item nav-link projects" href="#projects" data-toggle="tab"><FormattedMessage id="entity.tab.projects" defaultMessage="Projects" /></a> */}
							{((challenge.documents.length !== 0 && (!challenge.is_admin || !userContext.user)) || challenge.is_admin) && // hide documents tab is user is not admin and they are no docs
								<a className="nav-item nav-link" href="#documents" data-toggle="tab"><FormattedMessage id="entity.tab.documents" defaultMessage="Documents" /></a>
							}
              <a className="nav-item nav-link" href="#faq" data-toggle="tab"><FormattedMessage id="entity.tab.faq" defaultMessage="FAQ" /></a>
              {/* <a className="nav-item nav-link" href="#members" data-toggle="tab"><FormattedMessage id="entity.tab.participants" defaultMessage="Members" /></a> */}
              {/* <a className="nav-item nav-link" href="#followers" data-toggle="tab"><FormattedMessage id="entity.tab.followers" defaultMessage="Follower" /></a> */}
            </nav>
            <div className="tabContainer">
              <div className="tab-content justify-content-center container-fluid">
								<div className={newsPaneClasses} id="news">
									{/* Show feed, and pass DisplayCreate to all connected users */}
                  {challenge.feed_id && <Feed feedId={challenge.feed_id} displayCreate={this.context.connected ? true : false} isAdmin={challenge.is_admin} />}
                  {/* {challenge.feed_id && <Feed feedId={challenge.feed_id} displayCreate={this.context.connected ? challenge.is_member ? true : false : false} isAdmin={challenge.is_admin} />} */}
                </div>
                <div className={aboutPaneClasses} id="about">
                  <InfoHtmlComponent content={lang === "fr" && challenge.description_fr ? challenge.description_fr : challenge.description} />
                  <InfoInterestsComponent title={intl.formatMessage({id:'challenge.info.interests'})} content={challenge.interests} />
                  {/* <InfoSkillsComponent content={challenge.skills} /> */}
									<FormattedMessage id="user.profile.skills" defaultMessage="Skills">
									{skills => (<InfoSkillsComponent content={challenge.skills}
																										title={skills} />)}
                	</FormattedMessage>
                  {/* <FormattedMessage id="general.address" defaultMessage="Address">
                    {address => (<InfoAddressComponent  title={address}
                                                        address={challenge.address}
                                                        city={challenge.city}
                                                        country={challenge.country} />)}
                  </FormattedMessage> */}
                </div>
                {/* <div className="tab-pane" id="howto">
                  <InfoHtmlComponent title="" content={lang === "fr" && challenge.rules_fr ? challenge.rules_fr : challenge.rules} />
                </div> */}
                {/* <div className="tab-pane" id="projects">
                  <ListProjectsAttached itemId={challenge.id} itemType="challenges" />
                </div> */}
                <div className="tab-pane" id="documents">
                  <DocumentsManager item={challenge}
                                    itemId={challenge.id}
                                    itemType="challenges" />
                </div>
                <div className="tab-pane" id="faq">
                  <InfoHtmlComponent title="" content={lang === "fr" && challenge.faq_fr ? challenge.faq_fr : challenge.faq} />
                </div>
                {/* <div className="tab-pane" id="members">
									<PeopleList filter={true} listPeople={challenge.users} searchBar={false} itemType="challenges"/>
                </div> */}
                {/* <div className="tab-pane" id="followers">
                  <ListFollowers itemId={challenge.id} itemType="challenges" />
                </div> */}
              </div>
            </div>
          </div>
        }
      </Loading>
    );
  }
}
export default injectIntl(ChallengeDetails)
ChallengeDetails.contextType = UserContext;
