import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Redirect } from "react-router-dom";
import Api from 'Api';
import ChallengeForm from "./ChallengeForm";
import Loading from "Components/Tools/Loading";
import MembersList from "Components/Members/MembersList";
import DocumentsManager from "Components/Tools/Documents/DocumentsManager";
import ProjectsAttachedList from "Components/Projects/ProjectsAttachedList";
import { UserContext } from 'UserProvider';
import { stickyTabNav, scrollToActiveTab } from 'Components/Tools/utils/utils.js'

export default class ChallengeEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      challenge: this.props.challenge,
      challengeUpdated: false,
      failedLoad: false,
      loading: true,
      sending: false,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount(){
    if(this.props.challenge){
      this.setState({loading: false});
    } else {
      Api.get("/api/challenges/" + this.props.match.params.id )
      .then(res=>{
        // console.log("Challenge got : ", res.data);
        this.setState({challenge: res.data, loading: false});
      })
      .catch(error=>{
        // console.log(error);
        this.setState({failedLoad: true, loading: false});
      })
    }
  }

  componentWillReceiveProps(nextProps){
    const challenge = nextProps.challenge;
    this.setState({challenge});
  }

  handleChange(key, content){
    // console.log("Edit handleChange : " + key + " " + content);
		var newChallenge = this.state.challenge;
		// console.log(newChallenge);
    newChallenge[key] = content;
    // console.log(newChallenge);
    this.setState({ challenge: newChallenge });
  }

  handleSubmit(){
    const challenge = this.state.challenge;
    this.setState({sending: true});
    Api.patch("/api/challenges/" + this.state.challenge.id, {challenge})
    .then(res=>{
      // console.log(res);
      this.setState({challengeUpdated: true, sending: false});
    })
    .catch(error=>{
      // console.log(error);
      this.setState({sending: false});
    })
  }

  render(){
		const { challenge, challengeUpdated, failedLoad, loading, sending } = this.state;
		const challengeId = this.props.match.params.id
		let userContext = this.context;
		if(challenge || (!challenge && failedLoad)) { // check if challenge exist, or if it doesn't
			var urlBack = "/challenge/" + challenge.id;
			if(failedLoad || !userContext.user || !challenge.is_admin){ // redirect to projects page if not connected, not admin, if project doesn't exist, or was just deleted
				return( <Redirect to="/challenges" /> );
			}
		}
		setTimeout(function(){ 
			stickyTabNav("isEdit") // make the tab navigation bar sticky on top when we reach its scroll position
			scrollToActiveTab() // if there is a hash in the url and the tab exists, click and scroll to the tab
		}, 700); // had to add setTimeout for the function to work
    if(challengeUpdated){
      return(
        <Redirect push to={urlBack} />
      );
    }

    return(
      <Loading active={loading}>
        <div className="challengeEdit container-fluid">
          <h1><FormattedMessage id="challenge.edit.title" defaultMessage="Edit my Challenge"/></h1>
					<a href={urlBack}> {/* go back link*/}
						<i className="fa fa-arrow-left" />
						<FormattedMessage id="challenge.edit.back" defaultMessage="Go back"/>
					</a>
					<nav className="nav nav-tabs container-fluid">
						<a className="nav-item nav-link active" href="#basic_info" data-toggle="tab"><FormattedMessage id="entity.tab.basic_info" defaultMessage="Basic information" /></a>
	          <a className="nav-item nav-link" href="#members" data-toggle="tab"><FormattedMessage id="entity.tab.members" defaultMessage="Members" /></a>
	          <a className="nav-item nav-link" href="#projects" data-toggle="tab"><FormattedMessage id="projects" defaultMessage="Projects" /></a>
	          <a className="nav-item nav-link" href="#documents" data-toggle="tab"><FormattedMessage id="entity.tab.documents" defaultMessage="Documents" /></a>
	        </nav>
	        <div className="tabContainer">
	          <div className="tab-content justify-content-center container-fluid">
	            <div className="tab-pane active" id="basic_info">
								<ChallengeForm  challenge={challenge} handleChange={this.handleChange}
                          			handleSubmit={this.handleSubmit} mode="edit" sending={sending} />
	            </div>
	            <div className="tab-pane" id="members">
          			<MembersList itemType="challenges" itemId={challengeId} />
							</div>
							<div className="tab-pane" id="projects">
          			<ProjectsAttachedList itemType="challenges" itemId={challengeId} />
							</div>
							<div className="tab-pane" id="documents">
                <DocumentsManager item={challenge}
                                  itemId={challengeId}
                                  itemType="challenges" />
              </div>
						</div>
					</div>
        </div>
      </Loading>
    );
  }
}
ChallengeEdit.contextType = UserContext;