import React, { Component } from "react";
import ListComponent from "Components/Tools/ListComponent";
import ChallengeSearch from "./ChallengeSearch";
import { FormattedMessage } from "react-intl";

export default class ChallengeList extends Component {

  static get defaultProps() {
    return {
      listChallenges: [],
      searchBar: true,
      colMax: 3,
      displayNb: undefined,
      cardFormat: undefined,
    }
  }

  render(){
    const {colMax, listChallenges, searchBar, displayNb, cardFormat } = this.props;
    var listOfChallenges = displayNb ? listChallenges.slice(0, displayNb) : listChallenges

    return (
      <div className="challengeList">
        {displayNb && <h3><FormattedMessage id="challenges.latest" defaultMessage="Latest challenges" /></h3>}
        {/* {searchBar && <ChallengeSearch /> } */}
        <ListComponent itemType="challenge" data={listOfChallenges} colMax={colMax} cardFormat={cardFormat}/>
      </div>
    );
  }
}
