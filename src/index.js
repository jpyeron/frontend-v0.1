import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap';
import { addLocaleData, IntlProvider } from "react-intl";
import en from "react-intl/locale-data/en";
import fr from "react-intl/locale-data/fr";
import messages_en from "./translations/en";
import messages_fr from "./translations/fr";
import HttpsRedirect from 'react-https-redirect';

addLocaleData(en);
addLocaleData(fr);

const messages = {
    "en": messages_en,
    "fr": messages_fr
}
const language = getLanguage();

function getLanguage() {
    let lang;
    if(localStorage.getItem("language") !== null) {
        lang = localStorage.getItem("language");
    } else {
        lang = navigator.language.split(/[-_]/)[0];
    }
    return lang;
}
var element;
if(window.location.hostname !== "beta.jogl.io") { // if website is not beta.jogl.io, redirect to https (because beta don't support https)
	element = <HttpsRedirect><IntlProvider locale={language} key={language} messages={messages[language]}>
		<App />
	</IntlProvider></HttpsRedirect>
}
else { // else don't redirect
	element = <IntlProvider locale={language} key={language} messages={messages[language]}>
		<App />
	</IntlProvider>
}

ReactDOM.render(element, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
